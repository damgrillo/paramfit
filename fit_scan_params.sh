#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: fit_scan_params.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir
         -o  Output dir
         -p  Fitpar file
         -f  Force Field dir
         -b  Threshold for bond selection, default = 0.1
         -a  Threshold for angle selection, default = 30.0
         -d  Threshold for dihedral selection, default = 90.0
         -u  Include unconverged structures

DESCRIPTION:

         - The program performs parameter fitting using the data in the input dir
           and writes the results in the output dir.

         - Threshold values are used to select parameters based on their variability
           along the scans. When a parameter fluctuates more than its threshold value
           then it is included in the fit procedure. The fit results can be sensitive
           to these parameters, since lower or higher values can include or ignore
           parameters to be accounted in the calculation.

OPTIONS DETAILS:

         -i  Input dir must contain one subdirectory per scan, each one containing:

             -> PDB files extracted from the corresponding QM scan.

             -> PSF file defining the topology of the molecule (compatible for CHARMM or CGenFF).

             -> QM energy file in tabular format, in the first field the names of the PDB files,
                in the second field the scan parameter value for the corresponding PDB file, and   
                in the third field the QM energies in Hartree (qm_energies.dat). 

             -> MM energy file in tabular format, in the first field the names of the PDB files, and 
                in the second field the MM energies for each PDB in Kcal/mol. This file must correspond
                to the MM energies calculated using NAMD with initial FF parameters (mm_energies_init.dat) 
             
         -o  Output dir will contain: 

             -> Output file with the resulting fitted parameters in tabular format (fitres.dat).
                The parameters are written in the following format:
                
                * BONDS:  type1 type2 kb r0
                * ANGLES: type1 type2 type3 ka theta0
                * DIHEDS: type1 type2 type3 type4 kchi mult delta

             -> One subdirectory per scan (with the same name as in input dir), each one containing:

                * QM energy file in tabular format, in the first field the names of the PDB files, and 
                  in the second field their corresponding QM energies in Hartree (qm_energies.dat).   
   
                * MM energy files in tabular format, in the first field the names of the PDB files, and 
                  in the second field their corresponding MM energies in Kcal/mol. Three MM energy files
                  are saved: Initial parameters (mm_energies_init.dat) , fit parameters on zero 
                  (mm_energies_zero.dat) and new fitted parameters (mm_energies_new.dat) 

         -f  Force field dir must contain the corresponding RTF and PRM files for the desired FF
             (CHARMM or CGenFF). To avoid ambiguity, only one RTF and PRM files must be provided
             within this directory, else an error is reported.

         -p  Fitpar file must contain the parameters to be fitted. The file must be organized in
             a CVS format (comma separated values), in the first field the parameter type (mandatory), 
             in the second field a list of multiplicities (only for dihedrals, optional), and in the
             third field a keyword that indicates to keep phases fixed -keyword FIX- or allow them to
             vary -keyword VAR- (only for dihedrals, optional). If no multiplicities are defined,
             values 1, 2, 3, 4 are taken as default. If no phase keyword is indicated, FIX is taken
             as default value. Comments in a line can be inserted after # character.\n"

}

# Options parser
while getopts "i:o:p:e:f:b:a:d:u" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        o)
            OUTDIR=$OPTARG;;
        p)
            FITPARS=$OPTARG;; 
        e)
            EQVPARS=$OPTARG;; 
        f)
            FFDIR=$OPTARG;;  
        b)
            BTHRES=$OPTARG;;
        a)
            ATHRES=$OPTARG;;
        d)
            DTHRES=$OPTARG;;
        u)
            UNCONV='YES';;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" || -z "$OUTDIR" || -z "$FITPARS" || -z "$FFDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
elif [[ ! -d "$OUTDIR" ]]; then
    echo "Error: $OUTDIR is not a directory"; exit 1
elif [[ ! -f "$FITPARS" ]]; then
    echo "Error: $FITPARS is not a file"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi
if [[ "$(cd $INPDIR && pwd)" == "$(cd $OUTDIR && pwd)" ]]; then
    echo -n "Error: Input dir and output dir are the same. "
    echo "Please choose different location for output dir"
    exit 1
fi
if [[ -z "$BTHRES" ]]; then
    BTHRES=0.1
fi
if [[ -z "$ATHRES" ]]; then
    ATHRES=30.0
fi
if [[ -z "$DTHRES" ]]; then
    DTHRES=60.0
fi
if [ -z "$UNCONV" ]; then
    UNCONV='NO'
fi

INPDIR=${INPDIR%/}
OUTDIR=${OUTDIR%/}
FFDIR=${FFDIR%/}
FITRES=$OUTDIR/fit_results.dat

# Set fit parameters on zero and get energies
echo -e "\n# CALCULATE ENERGIES SETTING FIT PARAMETERS ON ZERO"
for DIR in $(ls -l $INPDIR | grep "^d" | awk '{print $9}' | grep "$DIRPATT"); do
    mkdir -p $OUTDIR/$DIR
    $(dirname $0)/get_energies_zero.sh -i $INPDIR/$DIR -o $OUTDIR/$DIR -p $FITPARS -f $FFDIR
done

# Fit parameters
echo -e "\n# FIT PARAMETERS"
ARGFILE=$OUTDIR/fitpar_args.txt
echo -n '' > $ARGFILE
for DIR in $(ls -l $INPDIR | grep "^d" | awk '{print $9}' | grep "$DIRPATT"); do
    PSF=$(ls $INPDIR/$DIR | grep '\.psf' | head -1)
    echo "$(cd $INPDIR/$DIR && pwd)" >> $ARGFILE
    cp $INPDIR/$DIR/qm_energies.dat $OUTDIR/$DIR/qm_energies.dat
    cat $OUTDIR/$DIR/qm_energies.dat | awk '{print $1, $3, $4}' | \
        join -j 1 - $OUTDIR/$DIR/mm_energies_zero.dat | grep -v '^\s*#' | \
        awk -v psf=$PSF '{printf "%-14s %-10s %16s %14s %12s\n", psf, $1, $2, $3, $4}' >> $ARGFILE
    echo >> $ARGFILE
done
echo -n "Fitting parameters ... "
START=$(date -u +%s.%N)
$(dirname $0)/scripts/fit_scan_params.py $ARGFILE $FITPARS $FITRES $BTHRES $ATHRES $DTHRES $UNCONV
END=$(date -u +%s.%N)
ELAPSED=$(echo "$END - $START" | bc -l | awk '{printf "%.2f\n", $0}')
echo "Elapsed time: $ELAPSED seconds"
rm $ARGFILE

# Set new fitted parameters and get energies
echo -e "\n# CALCULATE ENERGIES USING NEW FITTED PARAMETERS"
for DIR in $(ls -l $INPDIR | grep "^d" | awk '{print $9}' | grep "$DIRPATT"); do
    $(dirname $0)/get_energies_new.sh -i $INPDIR/$DIR -o $OUTDIR/$DIR -p $FITRES -f $FFDIR
done
echo


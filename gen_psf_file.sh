#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: gen_psf_file.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -w  Working dir containing PDB structures and topology STR file
         -f  Force Field dir

DESCRIPTION:

         - The program generates a topology PSF file into the working dir based on
           the PBD structures and the topology TOP file.

         - All the PDB structures in the working dir are supposed to have the same 
           topology, which must be defined in the TOP file inside the same dir.

         - The topology in the TOP file must be compatible for CHARMM or CGenFF.\n"
}

# Options parser
while getopts "w:f:" OPTION; do
    case $OPTION in
        w)
            WKDIR=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$WKDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$WKDIR" ]]; then
    echo "Error: $WKDIR is not a directory"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi

# NAMD dir
NAMD=$(dirname $0)/namd

# Clean files
if [ -n "$(ls $WKDIR | grep '_init\.' | grep -v 'energies')" ]; then
    rm $WKDIR/*_init.*
fi
if [ -n "$(ls $WKDIR | grep '_zero\.' | grep -v 'energies')" ]; then
    rm $WKDIR/*_zero.*
fi
if [ -n "$(ls $WKDIR | grep '_new\.' | grep -v 'energies') " ]; then
    rm $WKDIR/*_new.*
fi
if [ -n "$(ls $WKDIR | grep '\.psf')" ]; then
    rm $WKDIR/*.psf
fi

# Select TOP file
echo -n "Selecting TOP file in $WKDIR ... "
TOP=$(ls $WKDIR | grep '\.rtf$\|\.str$')
if [ -z "$TOP" ]; then
    echo
    echo "Error: No TOP file found in $WKDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $TOP | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several TOP files found in $WKDIR"
    echo "Exit program ..."
    exit 1
elif [ $(cat $WKDIR/$TOP | grep "RESI" | wc -l) -gt 1  ]; then
    echo $TOP
    echo "Error: Several residues defined within $TOP"
    echo "Exit program ..."
    rm -r $OUTDIR/$DIR
    exit 1
else
    echo "$TOP"
fi

# Select RTF files
echo -n "Selecting RTF file in $FFDIR ... "
RTF=$(ls $FFDIR | grep '\.rtf$')
if [ -z "$RTF" ]; then
    echo
    echo "Error: No RTF file found in $FFDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $RTF | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several RTF files found in $FFDIR"
    echo "Exit program ..."
    exit 1
else
    echo "$RTF"
fi

# Generate PSF file
echo -n "Generating PSF file in $WKDIR ..."
cp $NAMD/input.pgn $WKDIR
cp $FFDIR/$RTF $WKDIR
WKD=$(cd $WKDIR && pwd -P)
WKD=$(echo $WKD | sed 's/\//\\\//g')
PDB=$(ls $WKDIR | grep '\.pdb$' | head -1)
RES=$(cat $WKDIR/$PDB | grep 'ATOM\|HETATM' | head -1 | awk '{print $4}')
PSF=${TOP%.*}.psf
sed -i "s/%RTF%/$WKD\/$RTF/" $WKDIR/input.pgn
sed -i "s/%TOP%/$WKD\/$TOP/" $WKDIR/input.pgn
sed -i "s/%PDB%/$WKD\/$PDB/" $WKDIR/input.pgn
sed -i "s/%PSF%/$WKD\/$PSF/" $WKDIR/input.pgn
sed -i "s/%RES%/$RES/" $WKDIR/input.pgn
$NAMD/psfgen-2.10 < $WKDIR/input.pgn > $WKDIR/psfgen.log
rm $WKDIR/*.pgn 
rm $WKDIR/*.rtf
rm $WKDIR/*.log

# Test PSF file
if [ ! -f "$WKDIR/$PSF" ]; then
    echo -e "Error: PSF not generated\n"
    exit 1
else
    echo -e "$PSF\n"
fi


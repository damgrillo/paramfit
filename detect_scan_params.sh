#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: detect_scan_params.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -w  Working dir, containing PDB structures, QM energies and topology STR file
         -f  Force Field dir
         -b  Bond threshold, default = 0.1
         -a  Angle threshold, default = 40.0
         -d  Dihedral threshold, default = 90.0          

DESCRIPTION:

         - The program detects the scan parameter automatically based on parameter variability.
           The parameter which fluctuation along the scan is greater than a given threshold value is
           chosen as a candidate. Among them, the parameter with max variability is selected. 
           If no parameter is found or more than one parameter are identified, then an error message
           is displayed, indicating the possible options. Try to adjust the threshold values in order
           to detect the correct scan parameter. If detection is successful, then the corresponding
           QM energy file is updated including the scan parameter value in each line. 

         - The updated QM energy file (qm_energies.dat) will be in tabular format, in the first field
           the names of the PDB files, in the second field the scan parameter value for each PDB file,   
           and in the third field the QM energies in Hartree.  

         - All the PDB structures must correspond to the same topology, which must be defined in the STR file.

         - The topology must be compatible for CHARMM or CGenFF.

         - The RTF and PRM files for the selected FF (CHARMM or CGenFF) must be included in Force Field dir. 
           To avoid ambiguity, only one RTF and PRM files must be provided within this directory.\n"
}

# Options parser
while getopts "w:f:b:a:d:" OPTION; do
    case $OPTION in
        w)
            WKDIR=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        b)
            BTHRES=$OPTARG;;
        a)
            ATHRES=$OPTARG;;
        d)
            DTHRES=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$WKDIR" || -z "$FFDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$WKDIR" ]]; then
    echo "Error: $WKDIR is not a directory"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi
if [[ -z "$BTHRES" ]]; then
    BTHRES=0.1
fi
if [[ -z "$ATHRES" ]]; then
    ATHRES=40.0
fi
if [[ -z "$DTHRES" ]]; then
    DTHRES=90.0
fi

# Export path to extra python modules
export PYTHONPATH=$(dirname $0)/scripts

# Get scan parameters
echo "Detecting scan parameter in $WKDIR ..."
WKDIR=${WKDIR%/}
QMEFILE=$WKDIR/qm_energies.dat
PSFFILE=$WKDIR/$(ls $WKDIR | grep '\.psf$' | head -1)
for PDB in $(ls $WKDIR | grep '\.pdb' | sort); do
    PDBLIST="$PDBLIST $WKDIR/$PDB "
done
$(dirname $0)/scripts/detect_scan_params.py $BTHRES $ATHRES $DTHRES $QMEFILE $PSFFILE $PDBLIST
echo


#!/usr/bin/python

import re
import sys
import string
from itertools import izip
from paramtools import *

        
####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = include H parameters
# sys.argv[2]  = show bonds
# sys.argv[3]  = show angles
# sys.argv[4]  = show diheds
# sys.argv[5]  = psf file
# sys.argv[6+] = pdb file list

# Usage
if ((len(sys.argv) < 7) or 
    ((sys.argv[1] != 'YES') and (sys.argv[1] != 'NO')) or
    ((sys.argv[2] != 'YES') and (sys.argv[2] != 'NO')) or
    ((sys.argv[3] != 'YES') and (sys.argv[3] != 'NO')) or
    ((sys.argv[4] != 'YES') and (sys.argv[4] != 'NO'))):
    msg =  "\nUsage: show_params_stats.py [include_h] [show_bonds] [show_angles] [show_diheds] [psf_file] [pdb_file_list]\n"
    msg += "\ninclude_h:   YES, NO"
    msg += "\nshow_bonds:  YES, NO"
    msg += "\nshow_angles: YES, NO"
    msg += "\nshow_diheds: YES, NO\n"      
    sys.exit(msg)

# Include parameters involving H atoms
includeh = (sys.argv[1] == 'YES') 

# Show bonds, angles, diheds
showbonds  = (sys.argv[2] == 'YES') 
showangles = (sys.argv[3] == 'YES') 
showdiheds = (sys.argv[4] == 'YES') 

# Input files
psffile  = sys.argv[5]
pdblist  = sys.argv[6:]

# Get structure info from input files
chmlist = []
pdbnames = []
for pdbfile in pdblist:
    bondlist  = get_bonds_from_file(psffile)
    anglelist = get_angles_from_file(psffile)
    dihedlist = get_diheds_from_file(psffile)
    atomlist  = get_atoms_from_file(psffile, pdbfile)
    chmlist  += [ChemStruct(atomlist, bondlist, anglelist, dihedlist)]
    pdbnames += [pdbfile.rstrip('/').split('/')[-1]]

# Get bonds, angles and dihedral attributes for all structures grouped by index
bdattr = {idx:[] for idx, attr in chmlist[0].get_bonds_attr().items()}
anattr = {idx:[] for idx, attr in chmlist[0].get_angles_attr().items()}
dhattr = {idx:[] for idx, attr in chmlist[0].get_diheds_attr().items()}
for chm in chmlist:
    for idx, attr in chm.get_bonds_attr().items():
        typ, val = attr
        if not any(t.startswith('H') for t in typ.elems()) or includeh:
            bdattr[idx] += [attr]
    for idx, attr in chm.get_angles_attr().items():
        typ, val = attr
        if not any(t.startswith('H') for t in typ.elems()) or includeh:
            anattr[idx] += [attr]
    for idx, attr in chm.get_diheds_attr().items():
        typ, val = attr
        if not any(t.startswith('H') for t in typ.elems()) or includeh:
            dhattr[idx] += [attr]
bdattr = [(idx, attr) for idx, attr in bdattr.items() if len(attr) > 0]
anattr = [(idx, attr) for idx, attr in anattr.items() if len(attr) > 0]
dhattr = [(idx, attr) for idx, attr in dhattr.items() if len(attr) > 0]
bdattr = sorted(bdattr, key=lambda tup: tup[0])
anattr = sorted(anattr, key=lambda tup: tup[0])
dhattr = sorted(dhattr, key=lambda tup: tup[0])

# Print bond info
ncols = 6
if showbonds:
    print "\nBONDS:\n"

    np = 0
    while np < len(bdattr):

        sout = 'Filename'.ljust(15)
        for idx, attr in bdattr[np:np+ncols]:
            typ, val = attr[0] 
            sout += '_'.join(typ.elems()).rjust(20)
        print sout

        sout = ''.ljust(15)
        for idx, attr in bdattr[np:np+ncols]:
            typ, val = attr[0]
            sout += '_'.join(map(str, idx.elems())).rjust(20)
        print sout

        sout = ''
        for ns in range(len(pdbnames)):
            sout += pdbnames[ns].ljust(15)
            for battr in bdattr[np:np+ncols]:
                val = battr[1][ns][1]
                sout += "{:20.3f}".format(val)
            sout += '\n'
        print sout

        np += ncols

# Print angles info
ncols = 5
if showangles:
    print "\nANGLES:\n"

    np = 0
    while np < len(anattr):

        sout = 'Filename'.ljust(15)
        for idx, attr in anattr[np:np+ncols]:
            typ, val = attr[0] 
            sout += '_'.join(typ.elems()).rjust(25)
        print sout

        sout = ''.ljust(15)
        for idx, attr in anattr[np:np+ncols]:
            typ, val = attr[0]
            sout += '_'.join(map(str, idx.elems())).rjust(25)
        print sout

        sout = ''
        for ns in range(len(pdbnames)):
            sout += pdbnames[ns].ljust(15)
            for aattr in anattr[np:np+ncols]:
                val = aattr[1][ns][1]
                sout += "{:25.3f}".format(val)
            sout += '\n'
        print sout

        np += ncols

# Print dihedrals info
ncols = 4
if showdiheds:
    print "\nDIHEDRALS:\n"

    np = 0
    while np < len(dhattr):

        sout = 'Filename'.ljust(15)
        for idx, attr in dhattr[np:np+ncols]:
            typ, val = attr[0] 
            sout += '_'.join(typ.elems()).rjust(30)
        print sout

        sout = ''.ljust(15)
        for idx, attr in dhattr[np:np+ncols]:
            typ, val = attr[0]
            sout += '_'.join(map(str, idx.elems())).rjust(30)
        print sout

        sout = ''
        for ns in range(len(pdbnames)):
            sout += pdbnames[ns].ljust(15)
            for dattr in dhattr[np:np+ncols]:
                val = dattr[1][ns][1]
                sout += "{:30.3f}".format(val)
            sout += '\n'
        print sout

        np += ncols



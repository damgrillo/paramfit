#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: show_params_maxvar.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir containing PDB structures and topology STR file
         -f  Force Field dir (default: local version of CGenFF)   
         -b  Bond threshold, default = 0.1
         -a  Angle threshold, default = 40.0
         -d  Dihedral threshold, default = 90.0          

DESCRIPTION:

         - The program shows parameters with max variability > threshold.

         - All the PDB structures must correspond to the same topology, 
           which must be defined in the STR file.

         - The topology in the STR file must be compatible for CHARMM or CGenFF.\n"
}

# Options parser
while getopts "i:f:b:a:d:" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        b)
            BTHRES=$OPTARG;;
        a)
            ATHRES=$OPTARG;;
        d)
            DTHRES=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
fi
if [[ -z "$FFDIR" ]]; then
    FFDIR=$(dirname $0)/../toppar/cgenff
fi
if [[ -z "$BTHRES" ]]; then
    BTHRES=0.1
fi
if [[ -z "$ATHRES" ]]; then
    ATHRES=40.0
fi
if [[ -z "$DTHRES" ]]; then
    DTHRES=90.0
fi

# Clean files
if [ -n "$(ls $INPDIR | grep '_init\.')" ]; then
    rm $INPDIR/*_init.*
fi
if [ -n "$(ls $INPDIR | grep '_zero\.')" ]; then
    rm $INPDIR/*_zero.*
fi
if [ -n "$(ls $INPDIR | grep '_new\.')" ]; then
    rm $INPDIR/*_new.*
fi
if [ -n "$(ls $INPDIR | grep '\.psf')" ]; then
    rm $INPDIR/*.psf
fi

# Export path to extra python modules
export PYTHONPATH=$(dirname $0)/../scripts

# Select topology file
$(dirname $0)/../scripts/gen_psf_file.sh -w $INPDIR -f $FFDIR > /dev/null

# Select topology file
echo "Parameters with max variability in $INPDIR ... "
PSF=$(ls $INPDIR | grep '\.psf$')
if [ -z "$PSF" ]; then
    echo "Error: No PSF file found in $INPDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $PSF | wc -w) -gt 1 ]; then
    echo "Error: Several PSF files found in $INPDIR"
    echo "Exit program ..."
    exit 1
fi

# Get param stats
INPDIR=${INPDIR%/}
PSFFILE=$INPDIR/$PSF
for PDB in $(ls $INPDIR | grep '\.pdb' | sort); do
    PDBLIST="$PDBLIST $INPDIR/$PDB "
done
$(dirname $0)/show_params_maxvar.py $BTHRES $ATHRES $DTHRES $PSFFILE $PDBLIST


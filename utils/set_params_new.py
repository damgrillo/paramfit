#!/usr/bin/python

import re
import sys
import string
from itertools import izip
        
####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = input parameters file
# sys.argv[2]  = output parameters file
# sys.argv[3]  = new parameters file

# Usage
if len(sys.argv) != 4: 
    msg =  "\nUsage: set_params_new.py [input_param_file] [output_param_file] [new_params_file]\n"     
    sys.exit(msg)

# I/O files
inpfile = sys.argv[1]
outfile = sys.argv[2]
parfile = sys.argv[3]

# Patterns
intval    = '-?[0-9]+'
fltval    = '-?[0-9]+\.[0-9]*'
bondvals  = fltval + '\s+' + fltval + '\s*[^0-9]' 
anglevals = fltval + '\s+' + fltval + '\s*[^0-9]'
dihedvals = fltval + '\s+' + intval + '\s+' + fltval + '\s*[^0-9]'
bondtype  = '^([A-Za-z][A-Za-z0-9]+\s+){1,1}[A-Za-z][A-Za-z0-9]+'
angletype = '^([A-Za-z][A-Za-z0-9]+\s+){2,2}[A-Za-z][A-Za-z0-9]+'
dihedtype = '^([A-Za-z][A-Za-z0-9]+\s+){3,3}[A-Za-z][A-Za-z0-9]+'
bondpatt  = bondtype  + '\s+' + bondvals[:-9]  + '$'
anglepatt = angletype + '\s+' + anglevals[:-9] + '$'
dihedpatt = dihedtype + '\s+' + dihedvals[:-9] + '$'

# Load new parameters file
fpar = open(parfile, 'r')
parlist = [line.strip() for line in fpar if not line.lstrip().startswith('#')]
parlist = [par for par in parlist if len(par) > 0]
fpar.close()

# Separate bonds, angles, diheds
bondlist  = [par for par in parlist if re.match(bondpatt, par)]
anglelist = [par for par in parlist if re.match(anglepatt, par)]
dihedlist = [par for par in parlist if re.match(dihedpatt, par)]
noassign  = [par for par in parlist if par not in bondlist + anglelist + dihedlist]
if len(noassign) > 0:
    msg  = "\nError: Cannot set new parameters. "
    msg += "Invalid format for parameter " + noassign[0] + '\n'
    sys.exit(msg)
else:
    bondlist  = [par.split() for par in bondlist]
    anglelist = [par.split() for par in anglelist]
    dihedlist = [par.split() for par in dihedlist]

# Load input file text
filetext = []
finp = open(inpfile, 'r')
for line in finp:
    filetext.append(line.strip())
finp.close()

# Search bond, angles and dihedrals and skip these parameters
newtext = []
skippar = set([])
for line in filetext:
    found = False

    # Search bond match
    i = 0
    while i < len(bondlist) and not found:
        bond   = bondlist[i][:2]
        rbond  = list(reversed(bond))
        bpatt1 = '\s*' + '\s+'.join(bond)  + '\s+' + bondvals
        bpatt2 = '\s*' + '\s+'.join(rbond) + '\s+' + bondvals
        bstr1  = ''.join([typ.ljust(7) for typ in bond])
        bstr2  = ''.join([typ.ljust(7) for typ in rbond])
        if re.match(bpatt1 + '|' + bpatt2, line):
            found = True
            skippar.add(bstr1)
        elif re.match(bpatt2, line):
            found = True
            skippar.add(bstr2)
        else:
            newline = line
            i += 1

    # Search angle match
    i = 0
    while i < len(anglelist) and not found:
        angle  = anglelist[i][:3]
        rangle = list(reversed(angle))
        apatt1 = '\s*' + '\s+'.join(angle)  + '\s+' + anglevals
        apatt2 = '\s*' + '\s+'.join(rangle) + '\s+' + anglevals
        astr1  = ''.join([typ.ljust(7) for typ in angle])
        astr2  = ''.join([typ.ljust(7) for typ in rangle])
        if re.match(apatt1, line):
            found = True
            skippar.add(astr1)
        elif re.match(apatt2, line):
            found = True
            skippar.add(astr2)
        else:
            newline = line
            i += 1

    # Search dihed match
    i = 0
    while i < len(dihedlist) and not found:
        dihed  = dihedlist[i][:4]
        rdihed = list(reversed(dihed))
        dpatt1 = '\s*' + '\s+'.join(dihed)  + '\s+' + dihedvals
        dpatt2 = '\s*' + '\s+'.join(rdihed) + '\s+' + dihedvals
        dstr1  = ''.join([typ.ljust(7) for typ in dihed])
        dstr2  = ''.join([typ.ljust(7) for typ in rdihed])
        mult   = dihedlist[i][5]
        if re.match(dpatt1, line) and int(mult) == int(line.split()[5]):
            found = True
            skippar.add(dstr1)
        elif re.match(dpatt2, line) and int(mult) == int(line.split()[5]):
            found = True
            skippar.add(dstr2)
        else:
            newline = line
            i += 1

    # Add newline when match not found
    if not found:
        newtext.append(newline)

# Add new parameters
finaltext = []
for line in newtext:
    
    # Add bonds
    if line.strip().startswith('BONDS'):
        finaltext.append(line)
        for bond in bondlist:
            btyp    = ''.join([typ.ljust(7) for typ in bond[:2]])
            kb      = '{:.2f}'.format(float(bond[2])).rjust(7) 
            r0      = '{:.4f}'.format(float(bond[3])).rjust(11)
            commt   = ' ! New parameter'
            newline = btyp + kb + r0 + commt
            finaltext.append(newline)

    # Add angles
    elif line.strip().startswith('ANGLES'):
        finaltext.append(line)
        for angle in anglelist:
            atyp    = ''.join([typ.ljust(7) for typ in angle[:3]])
            ka      = '{:.2f}'.format(float(angle[3])).rjust(7) 
            th      = '{:.2f}'.format(float(angle[4])).rjust(10) 
            commt   = ' ! New parameter'
            newline = atyp + ka + th + commt
            finaltext.append(newline)

    # Add diheds
    elif line.strip().startswith('DIHEDRALS'):
        finaltext.append(line)
        for dihed in dihedlist:
            dtyp    = ''.join([typ.ljust(7) for typ in dihed[:4]])
            kchi    = '{:.4f}'.format(float(dihed[4])).rjust(10) 
            mult    = '{:.0f}'.format(float(dihed[5])).rjust(3)
            delta   = '{:.2f}'.format(float(dihed[6])).rjust(9) 
            commt   = ' ! New parameter'
            newline = dtyp + kchi + mult + delta + commt
            finaltext.append(newline)

    # Insert line
    else:
        finaltext.append(line)        
            
# Write output file
fout = open(outfile, 'w')
for line in finaltext:
    fout.write(line + '\n')
fout.close()


#!/usr/bin/python

import re
import sys
import string
from itertools import izip
        
####################################################################################
################################# MAIN PROGRAM #####################################
####################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = input parameters file
# sys.argv[2]  = output parameters file
# sys.argv[3]  = list of zero parameters, comma separated

# Usage
if len(sys.argv) != 4: 
    msg =  "\nUsage: set_params_zero.py [input_param_file] [output_param_file] [zero_param_list]\n"      
    sys.exit(msg)

# I/O files
inpfile = sys.argv[1]
outfile = sys.argv[2]

# Patterns
intval   = '-?[0-9]+'
fltval   = '-?[0-9]+\.[0-9]*'
bondvals  = fltval + '\s+' + fltval + '\s*[^0-9]' 
anglevals = fltval + '\s+' + fltval + '\s*[^0-9]'
dihedvals = fltval + '\s+' + intval + '\s+' + fltval + '\s*[^0-9]'
bondtype  = '^([A-Za-z][A-Za-z0-9]+\s+){1,1}[A-Za-z][A-Za-z0-9]+$'
angletype = '^([A-Za-z][A-Za-z0-9]+\s+){2,2}[A-Za-z][A-Za-z0-9]+$'
dihedtype = '^([A-Za-z][A-Za-z0-9]+\s+){3,3}[A-Za-z][A-Za-z0-9]+$'

# Load zero parameters
parstr    = sys.argv[3]
parlist   = [par.strip() for par in parstr.split(',') if len(par.strip()) > 0]
bondlist  = [par for par in parlist if re.match(bondtype, par)]
anglelist = [par for par in parlist if re.match(angletype, par)]
dihedlist = [par for par in parlist if re.match(dihedtype, par)]
noassign  = [par for par in parlist if par not in bondlist + anglelist + dihedlist]
if len(noassign) > 0:
    msg  = "\nError: Cannot set parameters on zero. "
    msg += "Invalid format for parameter " + noassign[0] + '\n'
    sys.exit(msg)
else:
    bondlist  = [par.split() for par in bondlist]
    anglelist = [par.split() for par in anglelist]
    dihedlist = [par.split() for par in dihedlist]

# Load input file text
filetext = []
finp = open(inpfile, 'r')
for line in finp:
    filetext.append(line.strip())
finp.close()

# Search bond, angles and dihedrals and replace values by zero
newtext = []
for line in filetext:
    found = False

    # Bonds
    i = 0
    while i < len(bondlist) and not found:
        bond   = bondlist[i]
        rbond  = list(reversed(bond))
        bpatt1 = '\s*' + '\s+'.join(bond)  + '\s+' + bondvals
        bpatt2 = '\s*' + '\s+'.join(rbond) + '\s+' + bondvals
        bstr1  = ''.join([typ.ljust(7) for typ in bond])
        bstr2  = ''.join([typ.ljust(7) for typ in rbond])
        r0     = '0.0000'.rjust(11) 
        kb     = '0.00'.rjust(7) 
        commt  = ' ! Zero parameters'
        if re.match(bpatt1, line):
            found = True
            newline = bstr1 + kb + r0 + commt
        elif re.match(bpatt2, line):
            found = True
            newline = bstr2 + kb + r0 + commt
        else:
            newline = line
        i += 1

    # Angles
    i = 0
    while i < len(anglelist) and not found:
        angle  = anglelist[i]
        rangle = list(reversed(angle))
        apatt1 = '\s*' + '\s+'.join(angle)  + '\s+' + anglevals
        apatt2 = '\s*' + '\s+'.join(rangle) + '\s+' + anglevals
        astr1  = ''.join([typ.ljust(7) for typ in angle])
        astr2  = ''.join([typ.ljust(7) for typ in rangle])
        th     = '0.00'.rjust(10) 
        ka     = '0.00'.rjust(7) 
        commt  = ' ! Zero parameters'
        if re.match(apatt1, line):
            found = True
            newline = astr1 + ka + th + commt
        elif re.match(apatt2, line):
            found = True
            newline = astr2 + ka + th + commt
        else:
            newline = line
        i += 1

    # Diheds
    i = 0
    while i < len(dihedlist) and not found:
        dihed  = dihedlist[i]
        rdihed = list(reversed(dihed))
        dpatt1 = '\s*' + '\s+'.join(dihed)  + '\s+' + dihedvals
        dpatt2 = '\s*' + '\s+'.join(rdihed) + '\s+' + dihedvals
        dstr1  = ''.join([typ.ljust(7) for typ in dihed])
        dstr2  = ''.join([typ.ljust(7) for typ in rdihed])
        kchi   = '0.0000'.rjust(10) 
        delta  = '0.00'.rjust(9) 
        commt  = ' ! Zero parameters'
        if re.match(dpatt1, line):
            found = True
            mult  = int(line.split()[5]) 
            newline = dstr1 + kchi + str(mult).rjust(3) + delta + commt
        elif re.match(dpatt2, line):
            found = True
            mult  = int(line.split()[5]) 
            newline = dstr1 + kchi + str(mult).rjust(3) + delta + commt
        else:
            newline = line
        i += 1

    # Add newline
    newtext.append(newline)    
            
# Write output file
fout = open(outfile, 'w')
for line in newtext:
    fout.write(line + '\n')
fout.close()


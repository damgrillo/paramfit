#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: show_params_stats.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir containing PDB structures and topology STR file
         -f  Force Field dir (default: local version of CGenFF)   
         -h  Include parameters involving H atoms
         -b  Show bonds info
         -a  Show angles info  
         -d  Show dihedrals info      

DESCRIPTION:

         - The program shows info about bonds, angles and dihedrals 
           in PDB structures in the input dir.

         - All the PDB structures must correspond to the same topology, 
           which must be defined in the STR file.

         - The topology in the STR file must be compatible for CHARMM or CGenFF.

         - Parameters involving H atoms are omitted by default. 
           To show them, include -h flag.

         - Bonds, angles and dihedrals info are all shown by default. 
           To show only specific types, include the corresponding flags.\n"
}

# Options parser
while getopts "i:f:hbad" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        h)
            INCLUDEH='YES';;
        b)
            SHOWBONDS='YES';;
        a)
            SHOWANGLES='YES';;
        d)
            SHOWDIHEDS='YES';;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
fi
if [[ -z "$FFDIR" ]]; then
    FFDIR=$(dirname $0)/../toppar/cgenff
fi
if [[ -z "$INCLUDEH" ]]; then
    INCLUDEH='NO'
fi
if [[ -z "$SHOWBONDS" && -z "$SHOWANGLES" && -z "$SHOWDIHEDS" ]]; then
    SHOWBONDS='YES'
    SHOWANGLES='YES'
    SHOWDIHEDS='YES'
fi
if [[ -z "$SHOWBONDS" ]]; then
    SHOWBONDS='NO'
fi
if [[ -z "$SHOWANGLES" ]]; then
    SHOWANGLES='NO'
fi
if [[ -z "$SHOWDIHEDS" ]]; then
    SHOWDIHEDS='NO'
fi

# Export path to extra python modules
export PYTHONPATH=$(dirname $0)/../scripts

# Select topology file
$(dirname $0)/../scripts/gen_psf_file.sh -w $INPDIR/$DIR -f $FFDIR > /dev/null

# Select topology file
echo "Bonds, angles and dihedrals info in $INPDIR ..."
PSF=$(ls $INPDIR | grep '\.psf$')
if [ -z "$PSF" ]; then
    echo "Error: No PSF file found in $INPDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $PSF | wc -w) -gt 1 ]; then
    echo "Error: Several PSF files found in $INPDIR"
    echo "Exit program ..."
    exit 1
fi

# Get param stats
INPDIR=${INPDIR%/}
PSFFILE=$INPDIR/$PSF
for PDB in $(ls $INPDIR | grep '\.pdb' | sort); do
    PDBLIST="$PDBLIST $INPDIR/$PDB "
done
$(dirname $0)/show_params_stats.py $INCLUDEH $SHOWBONDS $SHOWANGLES $SHOWDIHEDS $PSFFILE $PDBLIST


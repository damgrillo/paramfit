#!/usr/bin/python

import re
import sys
import string
from itertools import izip
from paramtools import *

        
##################################################################################################
######################################### MAIN PROGRAM ###########################################
##################################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = psf file
# sys.argv[2+] = pdb file list

# Usage
if len(sys.argv) < 6:
    msg =  "\nUsage: show_params_maxvar.py [bthres] [athres] [dthres] [psf_file] [pdb_file_list]\n"
    sys.exit(msg)

# Input files
bthres = float(sys.argv[1])
athres = float(sys.argv[2])
dthres = float(sys.argv[3])
psffile = sys.argv[4]
pdblist = sys.argv[5:]

# Get structure info from input files
chmlist = []
pdbnames = []
for pdbfile in pdblist:
    bondlist  = get_bonds_from_file(psffile)
    anglelist = get_angles_from_file(psffile)
    dihedlist = get_diheds_from_file(psffile)
    atomlist  = get_atoms_from_file(psffile, pdbfile)
    chmlist  += [ChemStruct(atomlist, bondlist, anglelist, dihedlist)]
    pdbnames += [pdbfile.rstrip('/').split('/')[-1]]

# Select scan parameters
parscan = ParamScan(chmlist)
bdfit = parscan.select_bonds(bthres)
anfit = parscan.select_angles(athres)
dhfit = parscan.select_diheds(dthres)

# Print scan parameter
for btyp, bmaxvar in bdfit.values():
    print ' '.join(btyp.elems()) + ' -> {0:.4f}'.format(bmaxvar)
for atyp, amaxvar in anfit.values():
    print ' '.join(atyp.elems()) + ' -> {0:.4f}'.format(amaxvar)
for dtyp, dmaxvar in dhfit.values():
    print ' '.join(dtyp.elems()) + ' -> {0:.4f}'.format(dmaxvar)



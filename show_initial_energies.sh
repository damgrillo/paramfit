#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: show_initial_energies.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -q  QM energy file
         -m  MM energy file 
         -u  Show energies for unconverged structures 

DESCRIPTION:

         - Show a QM-MM energy plot along the scan variable.\n"
}

# Options parser
while getopts "q:m:u" OPTION; do
    case $OPTION in
        q)
            QMEFILE=$OPTARG;;
        m)
            MMEFILE=$OPTARG;;
        u)
            UNCONV='YES';;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$QMEFILE" || -z "$MMEFILE" ]]; then
    usage; exit 1
elif [[ ! -f "$QMEFILE" ]]; then
    echo "Error: $QMEFILE is not a file"; exit 1
elif [[ ! -f "$MMEFILE" ]]; then
    echo "Error: $MMEFILE is not a file"; exit 1
fi
if [ -z "$UNCONV" ]; then
    UNCONV='NO'
fi

# Plot QM-MM energies
echo "Plotting QM-MM energies for $(dirname $QMEFILE) ..."
$(dirname $0)/scripts/show_initial_energies.py $QMEFILE $MMEFILE $UNCONV


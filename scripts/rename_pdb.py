#!/usr/bin/python

import re
import sys
import string
from itertools import izip

       
####################################################################################################
############################################### MAIN PROGRAM #######################################
####################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input str file
# sys.argv[2] = input pdb file
# sys.argv[3] = output pdb file

# Usage
if len(sys.argv) != 4:
    msg = "\nUsage: rename_pdb.py [input_str] [input_pdb] [output_pdb]\n"
    sys.stderr.write(msg)
    sys.exit() 

# Read input topology str
topresid = ''
topatoms = []
inpfile  = open(sys.argv[1], 'r')
for line in inpfile:
    if re.match('RESI\s+', line):
        topresid = line.split()[1]
    elif re.match('ATOM\s+', line):
        topatoms.append(line.split()[1])
inpfile.close()

# Read input pdb
soutlist = []
inpfile  = open(sys.argv[2], 'r')
for line in inpfile:
    if re.match('ATOM\s+|HETATM\s+', line):
        rtyp, anum, aname, resid, resnum, xc, yc, zc, occ, tf, asym  = line.split()[:11]
        aname = topatoms[int(anum)-1]
        resid = topresid
        sout =  '{0:6s}'.format(rtyp)      # Record type
        sout += '{0:>5s} '.format(anum)    # Atom number
        sout += '{0:>4s} '.format(aname)   # Atom name
        sout += '{0:5s}'.format(resid)     # Residue name
        sout += '{0:>4s} '.format(resnum)  # Residue number
        sout += '{0:>11s}'.format(xc)      # X-coord
        sout += '{0:>8s}'.format(yc)       # Y-coord
        sout += '{0:>8s}'.format(zc)       # Z-coord
        sout += '{0:>6s}'.format(occ)      # Occupancy
        sout += '{0:>6s}'.format(tf)       # Temp factor
        sout += '{0:>12s}'.format(asym)    # Element symbol
    else:
        sout = line.strip()
    soutlist.append(sout)
inpfile.close()

# Write output pdb
outfile = open(sys.argv[3], 'w')
for sout in soutlist:
    outfile.write(sout + '\n')
outfile.close()


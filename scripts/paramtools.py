#!/usr/bin/python

import re
import sys
import math
import string
import numpy as np
from itertools import izip
from collections import OrderedDict, Iterable
from scipy.optimize import leastsq, minimize


##########################################################################################
##################################### ENERGY FUNCTIONS ###################################
##########################################################################################

# Energy function for bond terms:
# -------------------------------   
# Ebond = sum { kb * (bval - b0)^2 } (bonds in angstrom)
#          b 

# Energy function for angle terms:
# --------------------------------   
# Eangle = sum { ka * (aval - a0)^2 } (angles in radians)
#           a 

# Energy function for dihedral terms:
# -----------------------------------   
# Edih = sum { sum { kchi_dm * (1 + cos(m * dval - delta_dm)) } (diheds in radians)
#         d     m


##########################################################################################
############################ ENERGY MODELS FOR PARAMETER FITTING #########################
##########################################################################################

# Energy function for bond terms:
# -------------------------------   
# Ebond =     sum    { k(t) * [O(t) * (b(t) - B(t))^2 + C(t)] }
#          t in type
#
# where: B(t) = S(t) / O(t)
#        C(t) = (Q(t) * O(t) - S(t)^2) / O(t) 
#        Q(t) = sum over bval of type t { bval^2 } 
#        S(t) = sum over bval of type t { bval } 
#        O(t) = sum over bval of type t { 1 } 
#        k(t) = parameter kb of type t
#        b(t) = parameter b0 of type t
#
# Units: bval and b(t) in angstrom, k(t) in kcal/mole/angstrom^2


# Energy function for angle terms:
# --------------------------------   
# Eangle =     sum    { k(t) * [O(t) * (a(t) - A(t))^2 + C(t)] }
#          t in type
#
# where: A(t) = S(t) / O(t)
#        C(t) = (Q(t) * O(t) - S(t)^2) / O(t) 
#        Q(t) = sum over aval of type t { aval^2 } 
#        S(t) = sum over aval of type t { aval } 
#        O(t) = sum over aval of type t { 1 } 
#        k(t) = parameter ka of type t
#        a(t) = parameter a0 of type t
#
# Units: aval and a(t) in rad, k(t) in kcal/mole/rad^2


# Energy function for dihedral terms:
# -----------------------------------   
# Edih =     sum    {     sum    { C(t,m) * A(t,m) + S(t,m) * B(t,m) }  }
#         t in type   m in mults
#
# where: C(t,m) = sum over dval of type t { cos(m * dval) } 
#        S(t,m) = sum over dval of type t { sin(m * dval) } 
#        A(t,m) = k(t,m) * cos(d(t,m))
#        B(t,m) = k(t,m) * sin(d(t,m))
#        k(t,m) = parameter kchi of type t, mult m
#        d(t,m) = parameter delta of type t, mult m
#
# Units: dval, d(t,m) in rad, k(t) in kcal/mole/rad
#
# Once obtained A(t,m) and B(t,m), the parameters k(t,m) and d(t,m) can be calculated by
# 
#        k(t,m) = sqrt(A(t,m)^2 + B(t,m)^2) 
#        d(t,m) = atan(B(t,m) / A(t,m)) 
#
# If fixed phase 0 or 180 is chosen, B(t,m) = 0 and only A(t,m) is needed to be fitted


#########################################################################################
######################################### CLASSES #######################################
#########################################################################################

# Vect3D Class
class Vect3D:

    # Model for Vect3D:
    # -----------------
    # coords = Vector 3D Coordinates = tuple(float), length = 3

    # Initialize object
    def __init__(self, coords):
        if len(coords) != 3:
            raise ValueError("Error: Bad size for coords")
        self.coords = tuple(float(c) for c in coords)

    # Get coords
    def coords(self):
        return self.coords

    # Get difference
    def diff(self, v):
        return Vect3D(tuple((c1 - c2) for c1, c2 in izip(self.coords, v.coords)))

    # Get dot product
    def dot(self, v):
        return sum([c1 * c2 for c1, c2 in izip(self.coords, v.coords)])

    # Get cross product
    def cross(self, v):
        c1 = self.coords[1] * v.coords[2] - self.coords[2] * v.coords[1]
        c2 = self.coords[2] * v.coords[0] - self.coords[0] * v.coords[2]
        c3 = self.coords[0] * v.coords[1] - self.coords[1] * v.coords[0]
        return Vect3D((c1, c2, c3))

    # Get norm
    def norm(self): 
        return math.sqrt(self.dot(self))

    # Get distance
    def dist(self, v):
        return self.diff(v).norm()


# Atom Class
class Atom:

    # Model for Atom:
    # ---------------
    # typ = atom type = string
    # pos = atom position = Vect3D

    # Initialize object
    def __init__(self, typ, coords):
        self.typ = str(typ)
        self.pos = Vect3D(coords)

    # Get atom type
    def gettype(self):
        return self.typ

    # Get atom position
    def getpos(self):
        return self.pos


# BaseTuple Class
# Used to represent bond, angle or dihedral interactions
# Each BaseTuple stores indexes or types of each atom involved in the interaction 
class BaseTuple:

    # Model for BaseTuple:
    # --------------------
    # tuple = atom indexes = tuple(int)
    #       = atom types   = tuple(str)

    # Initialize object
    def __init__(self, tup):
        self.tup = tuple(tup)

    # Equal method
    def __eq__(self, tb):
        return self.tup == tb.tup

    # Not equal method
    def __ne__(self, tb):
        return not tb == self

    # Greater than method
    def __gt__(self, tb):
        return self.tup > tb.tup

    # Greater equal method
    def __ge__(self, tb):
        return self.tup >= tb.tup

    # Less than method
    def __lt__(self, tb):
        return self.tup < tb.tup

    # Less equal method
    def __le__(self, tb):
        return self.tup <= tb.tup

    # Hash method
    def __hash__(self):
        return hash(self.tup)

    # Test equivalence
    def iseqv(self, tb):
        return (self == tb) or (self == tb.reverse()) 

    # Test membership 
    def ismember(self, c):
        return (self in c) or (self.reverse() in c)

    # Get member 
    def getmember(self, c):
        if self in c:
            return self
        elif self.reverse() in c:
            return self.reverse()
        else:
            return None

    # Get reverse 
    def reverse(self):
        return BaseTuple(tuple(reversed(self.tup)))

    # Get elements
    def elems(self):
        return self.tup


# BondIndex Class
class BondIndex(BaseTuple):

    # Model for BondIndex:
    # --------------------
    # tuple = bond atoms indexes = tuple(int), length = 2

    # Initialize object
    def __init__(self, btuple):
        if len(btuple) != 2:
            raise ValueError("Error: bad size for btuple")
        self.tup = tuple(int(i) for i in btuple)

    # Get reverse bond
    def reverse(self):
        return BondIndex(tuple(reversed(self.tup)))


# AngleIndex Class
class AngleIndex(BaseTuple):

    # Model for AngleIndex:
    # --------------------
    # tuple = angle atoms indexes = tuple(int), length = 3

    # Initialize object
    def __init__(self, atuple):
        if len(atuple) != 3:
            raise ValueError("Error: bad size for atuple")
        self.tup = tuple(int(i) for i in atuple)

    # Get reverse angle
    def reverse(self):
        return AngleIndex(tuple(reversed(self.tup)))


# DihedIndex Class
class DihedIndex(BaseTuple):

    # Model for DihedIndex:
    # --------------------
    # tuple = dihedral atoms indexes = tuple(int), length = 4

    # Initialize object
    def __init__(self, dtuple):
        if len(dtuple) != 4:
            raise ValueError("Error: bad size for dtuple")
        self.tup = tuple(int(i) for i in dtuple)

    # Get reverse dihedral
    def reverse(self):
        return DihedIndex(tuple(reversed(self.tup)))


# BondType Class
class BondType(BaseTuple):

    # Model for BondType:
    # --------------------
    # tuple = bond atoms types = tuple(str), length = 2

    # Initialize object
    def __init__(self, btuple):
        if len(btuple) != 2:
            raise ValueError("Error: bad size for btuple")
        self.tup = tuple(str(i) for i in btuple)

    # Get reverse bond
    def reverse(self):
        return BondType(tuple(reversed(self.tup)))


# AngleType Class
class AngleType(BaseTuple):

    # Model for AngleType:
    # --------------------
    # tuple = angle atoms types = tuple(str), length = 3

    # Initialize object
    def __init__(self, atuple):
        if len(atuple) != 3:
            raise ValueError("Error: bad size for atuple")
        self.tup = tuple(str(i) for i in atuple)

    # Get reverse angle
    def reverse(self):
        return AngleType(tuple(reversed(self.tup)))


# DihedType Class
class DihedType(BaseTuple):

    # Model for DihedType:
    # --------------------
    # tuple = dihedral atoms types = tuple(str), length = 4

    # Initialize object
    def __init__(self, dtuple):
        if len(dtuple) != 4:
            raise ValueError("Error: bad size for dtuple")
        self.tup = tuple(str(i) for i in dtuple)

    # Get reverse dihedral
    def reverse(self):
        return DihedType(tuple(reversed(self.tup)))


# ChemStruct Class
class ChemStruct:

    # Model for ChemStruct:
    # ---------------------
    # qme    = QM energy = float
    # mme    = MM energy = float
    # atoms  = atoms in structure = dict{int : Atom}
    # bonds  = bonds in structure = set(BondIndex)
    # angles = angles in structure = set(AngleIndex)
    # diheds = dihedrals in structure = set(DihedIndex)

    # Initialize object
    def __init__(self, atoms, bonds=[], angles=[], diheds=[], qme=0.0, mme=0.0):
        if (not all(isinstance(idx, int) for idx in atoms.keys()) or
            not all(isinstance(at, Atom) for at in atoms.values())):
            raise ValueError("Error: bad types for atoms")
        if not all(isinstance(bd, BondIndex) for bd in bonds):
            raise ValueError("Error: bad types for bonds")
        if not all(isinstance(an, AngleIndex) for an in angles):
            raise ValueError("Error: bad types for angles")
        if not all(isinstance(dh, DihedIndex) for dh in diheds):
            raise ValueError("Error: bad types for diheds")        
        self.atoms  = atoms
        self.qme    = float(qme)
        self.mme    = float(mme)
        self.bonds  = set([min(bd, bd.reverse()) for bd in bonds])
        self.angles = set([min(an, an.reverse()) for an in angles])
        self.diheds = set([min(dh, dh.reverse()) for dh in diheds])

    # Get QM energy
    def get_qme(self):
        return self.qme

    # Get MM energy
    def get_mme(self):
        return self.mme

    # Get atoms
    def get_atoms(self):
        return self.atoms

    # Get bonds attributes:
    # ---------------------
    # Returns: bonds indexes, types and values = dict{BondIndex : (BondType, float)}
    def get_bonds_attr(self):
        battr = {}
        for bd in self.bonds:
            btyp = BondType(tuple(self.atoms[idx].gettype() for idx in bd.elems()))
            bval = self._get_bval(bd)
            battr[bd] = (btyp, bval)
        return battr

    # Get angles attributes:
    # ----------------------
    # Returns: angles indexes, types and values = dict{AngleIndex : (AngleType, float)}
    def get_angles_attr(self):
        aattr = {}
        for an in self.angles:
            atyp = AngleType(tuple(self.atoms[idx].gettype() for idx in an.elems()))
            aval = self._get_aval(an)
            aattr[an] = (atyp, aval)
        return aattr

    # Get dihedrals attributes:
    # -------------------------
    # Returns: dihedrals indexes, types and values = dict{DihedIndex : (DihedType, float)}
    def get_diheds_attr(self):
        dattr = {}
        for dh in self.diheds:
            dtyp = DihedType(tuple(self.atoms[idx].gettype() for idx in dh.elems()))
            dval = self._get_dval(dh)
            dattr[dh] = (dtyp, dval)
        return dattr

    # Get bonds energy:
    # -----------------
    # Parameters: bdpars = bonds parameters = dict{BondType : (float, float)}
    # Returns:    bdener = bonds energy = float
    def get_bonds_energy(self, bdpars):
        bdener = 0
        for btyp, bval in self.get_bonds_attr().values():
            if btyp.ismember(bdpars):
                kb, b0 = bdpars[btyp.getmember(bdpars)]
                bdener += kb * (bval - b0) ** 2 
        return bdener

    # Get angles energy:
    # -----------------
    # Parameters: anpars = angles parameters = dict{AngleType : (float, float)}
    # Returns:    anener = angles energy = float
    def get_angles_energy(self, anpars):
        anener = 0
        for atyp, aval in self.get_angles_attr().values():
            if atyp.ismember(anpars):
                ka, a0 = anpars[atyp.getmember(anpars)]
                anener += ka * ((aval - a0) * math.pi / 180) ** 2 
        return anener

    # Get dihedrals energy:
    # ---------------------
    # Parameters: dhpars = dihedrals parameters = dict{DihedType : dict{int: (float, float)}}
    # Returns:    dhener = dihedrals energy = float
    def get_diheds_energy(self, dhpars):
        dhener = 0
        for dtyp, dval in self.get_diheds_attr().values():
            if dtyp.ismember(dhpars):
                for mult, pars in dhpars[dtyp.getmember(dhpars)].items():
                    kchi, delta = pars
                    dhener += kchi * (1 + math.cos((mult * dval - delta) * math.pi / 180)) 
        return dhener

    # Internal method: get bond value
    def _get_bval(self, bond):
        p1, p2 = [self.atoms[idx].getpos() for idx in bond.elems()]
        return p1.diff(p2).norm()

    # Internal method: get angle value
    def _get_aval(self, angle):
        p1, p2, p3 = [self.atoms[idx].getpos() for idx in angle.elems()]
        v12 = p1.diff(p2)
        v32 = p3.diff(p2)
        vn  = v32.cross(v12)
        cos_phi = v12.dot(v32) / (v12.norm() * v32.norm())
        if math.fabs(cos_phi) > 1:
            cos_phi -= cos_phi * 1e-10
        return (math.acos(cos_phi) * 180 / math.pi)

    # Internal method: get dihedral value
    def _get_dval(self, dihed):
        p1, p2, p3, p4 = [self.atoms[idx].getpos() for idx in dihed.elems()]
        v12 = p1.diff(p2)
        v32 = p3.diff(p2)
        v23 = p2.diff(p3)
        v43 = p4.diff(p3)
        n1 = v12.cross(v32)
        n2 = v23.cross(v43)
        cos_phi = n1.dot(n2) / (n1.norm() * n2.norm())
        if math.fabs(cos_phi) > 1:
            cos_phi -= cos_phi * 1e-10
        if v43.dot(n1) > 0:
            return (360 - math.acos(cos_phi) * 180 / math.pi)
        else:
            return (math.acos(cos_phi) * 180 / math.pi)


# ParamScan Class
class ParamScan:

    # Model for ParamScan:
    # --------------------
    # chmstr = chemical structures in the scan = list(ChemStruct)

    # Initialize object:
    def __init__(self, chmstr):
        if not all(isinstance(cs, ChemStruct) for cs in chmstr):
            raise ValueError("Error: bad types for chmstr")
        self.chmstr = list(chmstr)

    # Automatic parameter selection:
    # ------------------------------
    # Methods: Search for parameters (bonds, angles, dihedrals) based on their fluctuations over the structures.
    #          A parameter is selected when its fluctuation is greater than its threshold value (bthres, athres, dthres).
    #          The parameter selection can be restrained to a list of specific parameter types (bdrest, anrest, dhrest).
    #          By default, parameter selection is not restricted to any type (bdrest, anrest, dhrest = None).
    #          If parameter threshold is set to 0, then all parameters matching the specified types are selected. 
    #          In addition, equivalence between parameter types can be introduced (bdeqv, aneqv, dheqv).
    #          Parameters involving H atoms are always ignored.
    #

    # Bonds selection:
    # ----------------
    # Parameters: bthres  = float
    #             bdrest = Iterable(BondType)
    #             bdeqv  = dict{BondType : BondType} 
    #
    # Returns: indexes, types and fluctuation for selected bonds = dict{BondIndex : (BondType, float)}
    def select_bonds(self, bthres, bdrest=None, bdeqv={}):

        # Select bond types and values based on bdrest and bdeqv
        if isinstance(bdrest, Iterable): 
            bdrest = set(bdrest)  
        bdtype = {bd:None for cs in self.chmstr for bd in cs.get_bonds_attr()}
        bdvals = {bd:[]   for cs in self.chmstr for bd in cs.get_bonds_attr()}
        bdcnts = {bd:0    for cs in self.chmstr for bd in cs.get_bonds_attr()}
        for cs in self.chmstr:
            for bd, attr in cs.get_bonds_attr().items():
                bt, bv = attr
                be = get_eqvtype(bt, bdeqv)         
                if (bdtype[bd] is not None) and not bt.iseqv(bdtype[bd]):
                    raise ValueError("Bad bond type! All structures in scan must have the same topology")
                if (bdrest is None) or bt.ismember(bdrest) or be.ismember(bdrest):   
                    bdtype[bd] =  bt 
                    bdvals[bd] += [bv]
                    bdcnts[bd] += 1          
                        
        # Select bonds based on bthres
        bdsel = {}
        for bd, bvs in bdvals.items():
            if (len(bvs) > 0) and not any(atyp.startswith('H') for atyp in bdtype[bd].elems()):
                maxdiff = max([math.fabs(v1 - v2) for v1 in bvs for v2 in bvs])
                if maxdiff > bthres:
                    bdsel[bd] = (bdtype[bd], maxdiff)
        return bdsel

    # Angles selection:
    # -----------------
    # Parameters: athres  = float
    #             anrest = Iterable(AngleType)
    #             aneqv  = dict{AngleType : AngleType} 
    #
    # Returns: indexes, types and fluctuation for selected angles = dict{AngleIndex : (AngleType, float)}
    def select_angles(self, athres, anrest=None, aneqv={}):

        # Select angle types and values based on anrest and aneqv
        if isinstance(anrest, Iterable): 
            anrest = set(anrest)  
        antype = {an:None for cs in self.chmstr for an in cs.get_angles_attr()}
        anvals = {an:[]   for cs in self.chmstr for an in cs.get_angles_attr()}
        ancnts = {an:0    for cs in self.chmstr for an in cs.get_angles_attr()}
        for cs in self.chmstr:
            for an, attr in cs.get_angles_attr().items():
                at, av = attr
                ae = get_eqvtype(at, aneqv)         
                if (antype[an] is not None) and not at.iseqv(antype[an]):
                    raise ValueError("Bad angle type! All structures in scan must have the same topology")
                if (anrest is None) or at.ismember(anrest) or ae.ismember(anrest):   
                    antype[an] =  at 
                    anvals[an] += [av * math.pi / 180]
                    ancnts[an] += 1          
                        
        # Select angles based on athres
        ansel = {}
        for an, avs in anvals.items():
            if (len(avs) > 0) and not any(atyp.startswith('H') for atyp in antype[an].elems()):
                maxdiff = max([math.acos(math.cos(v1 - v2)) for v1 in avs for v2 in avs]) * 180 / math.pi
                if maxdiff > athres:
                    ansel[an] = (antype[an], maxdiff)
        return ansel

    # Dihedrals:
    # ----------
    # Parameters: dthres  = float
    #             dhrest = Iterable(DihedType)
    #             dheqv  = dict{DihedType : DihedType} 
    #
    # Returns: indexes, types and fluctuation for selected dihedrals = dict{DihedIndex : (DihedType, float)}
    def select_diheds(self, dthres, dhrest=None, dheqv={}):

        # Select dihedral types and values based on dhrest and dheqv
        if isinstance(dhrest, Iterable): 
            dhrest = set(dhrest)  
        dhtype = {dh:None for cs in self.chmstr for dh in cs.get_diheds_attr()}
        dhvals = {dh:[]   for cs in self.chmstr for dh in cs.get_diheds_attr()}
        dhcnts = {dh:0    for cs in self.chmstr for dh in cs.get_diheds_attr()}
        for cs in self.chmstr:
            for dh, attr in cs.get_diheds_attr().items():
                dt, dv = attr
                de = get_eqvtype(dt, dheqv)         
                if (dhtype[dh] is not None) and not dt.iseqv(dhtype[dh]):
                    raise ValueError("Bad dihedral type! All structures in scan must have the same topology")
                if (dhrest is None) or dt.ismember(dhrest) or de.ismember(dhrest):   
                    dhtype[dh] =  dt 
                    dhvals[dh] += [dv * math.pi / 180]
                    dhcnts[dh] += 1          
                        
        # Select dihedrals based on dthres
        dhsel = {}
        for dh, dvs in dhvals.items():
            if (len(dvs) > 0) and not any(atyp.startswith('H') for atyp in dhtype[dh].elems()):
                maxdiff = max([math.acos(math.cos(v1 - v2)) for v1 in dvs for v2 in dvs]) * 180 / math.pi
                if maxdiff > dthres:
                    dhsel[dh] = (dhtype[dh], maxdiff)
        return dhsel

    # Get chemical structures
    def get_chmstr(self):
        return self.chmstr


# ParamFit Class
class ParamFit:

    # Model for ParamFit:
    # -------------------
    # parscans = scans for different parameters = list(ParamScan)
    # bdtypes  = bond types to be fitted  = list(BondType)
    # antypes  = angle types to be fitted = list(AngleType)
    # dhtypes  = dihed types to be fitted = list(DihedType)
    # bdeqv    = equivalent bonds  = dict{BondType : BondType}
    # aneqv    = equivalent angles = dict{AngleType : AngleType}
    # dheqv    = equivalent diheds = dict{DihedType : DihedType}
    # dhmult   = dihed multiplicities = dict{DihedType : list(int)}
    # dhflag   = dihed phase flag = dict{DihedType : str}
    # sysarray = system array = Numpy Array 
 
    # Initialize object:
    def __init__(self, parscans, bdtypes=[], antypes=[], dhtypes=[], 
                       bdeqv={}, aneqv={}, dheqv={}, dhmult={}, dhflag={}):
        if not all(isinstance(ps, ParamScan) for ps in parscans):
            raise ValueError("Error: bad types for parscans")
        if not all(isinstance(bt, BondType) for bt in bdtypes):
            raise ValueError("Error: bad types for bdtypes")
        if not all(isinstance(at, AngleType) for at in antypes):
            raise ValueError("Error: bad types for antypes")
        if not all(isinstance(dt, DihedType) for dt in dhtypes):
            raise ValueError("Error: bad types for dhtypes")
        if (not all(isinstance(btk, BondType) for btk in bdeqv.keys()) or
            not all(isinstance(btv, BondType) for btv in bdeqv.values())):
            raise ValueError("Error: bad types for bdeqv")
        if (not all(isinstance(atk, AngleType) for atk in aneqv.keys()) or
            not all(isinstance(atv, AngleType) for atv in aneqv.values())):
            raise ValueError("Error: bad types for aneqv")
        if (not all(isinstance(dtk, DihedType) for dtk in dheqv.keys()) or
            not all(isinstance(dtv, DihedType) for dtv in dheqv.values())):
            raise ValueError("Error: bad types for dheqv")
        if (not all(isinstance(dt, DihedType) for dt in dhmult.keys()) or
            not all(isinstance(m,  int) for mult in dhmult.values() for m in mult)):
            raise ValueError("Error: bad types for dhmult")
        if (not all(isinstance(dt, DihedType) for dt in dhflag.keys()) or
            not all(flag in ['FIX', 'VAR'] for flag in dhflag.values())):
            raise ValueError("Error: bad types for dhflag")

        # Define multiplicities and phase flags
        # Default values: mult = [1, 2, 3, 4], flag = 'FIX' 
        dhtypes = set([min(dh, dh.reverse()) for dh in dhtypes])
        for dt in dhtypes:
            if dt.ismember(dhmult) and len(dhmult[dt.getmember(dhmult)]) > 0:
                dhmult[dt.getmember(dhmult)].sort()
            else:
                dhmult[dt] = [1, 2, 3, 4]
        for dt in dhtypes:
            if not dt.ismember(dhflag):
                dhflag[dt] = 'FIX'

        # Initialize ParamFit
        self.parscans = list(parscans)
        self.bdtypes  = set([min(bd, bd.reverse()) for bd in bdtypes])
        self.antypes  = set([min(an, an.reverse()) for an in antypes])
        self.dhtypes  = set([min(dh, dh.reverse()) for dh in dhtypes])
        self.bdeqv    = bdeqv
        self.aneqv    = aneqv
        self.dheqv    = dheqv
        self.dhmult   = dhmult
        self.dhflag   = dhflag
        self.sysarray = np.zeros((0, 0))


    # Fit parameters:
    # ---------------
    # Parameters: bthres = bond threshold  = float
    #             athres = angle threshold = float
    #             dthres = dihed threshold = float
    #
    # Returns: (kb, b0) group by bond type = dict{BondType : tuple(float, float)}
    #          (ka, a0) group by angle type = dict{AngleType : tuple(float, float)}
    #          (mult, kchi, delta) group by dihedral type = dict{DihedType : list(tuple(int, float, float))}
    def fit(self, bthres, athres, dthres):

        # Calculate QM - MM energy difference and energy weights
        # QM and MM energies are assumed to be in Hartree and Kcal/mol respectively 
        # To calculate difference properly, both energies must be in the same scale. 
        # To this end, convert QM from Hartree to Kcal/mol (qconv = 627.503)
        qconv = 627.503
        eqmmm = np.array([])
        ewgts = np.array([])
        for ps in self.parscans:
            qme  = np.array([cs.get_qme() for cs in ps.get_chmstr()]) * qconv
            mme  = np.array([cs.get_mme() for cs in ps.get_chmstr()])
            qme -= np.amin(qme)
            mme -= np.amin(mme)
            edf  = qme - mme
            ewg  = np.full(edf.shape, 1.0 / math.sqrt(np.amax(edf) - np.amin(edf)))
            ewgts = np.append(ewgts, ewg)
            eqmmm = np.append(eqmmm, edf)

        # Perform a first step using SLSQP method and forcing fixed phases for all dihedrals
        ffix = True
        self._calc_sysarray(bthres, athres, dthres)
        minargs = (eqmmm, ewgts, ffix)
        iguess  = self._calc_iguess()
        fitres  = minimize(self._sqediff, iguess, args=minargs, method='SLSQP')
        bdpars, anpars, dhpars = self._get_fit_params(fitres.x, ffix)

        # Now perform a new step using COBYLA method and allowing corresponding phases to vary
        if any(flag == 'VAR' for flag in self.dhflag.values()):
            ffix = False
            minargs = (eqmmm, ewgts, ffix)
            iguess = self._calc_iguess(ipars=dict(bdpars.items() + anpars.items() + dhpars.items()))
            fitres = minimize(self._sqediff, iguess, args=minargs, method='COBYLA')
            bdpars, anpars, dhpars = self._get_fit_params(fitres.x, ffix)

        # Return fitted parameters
        return (bdpars, anpars, dhpars)


    # Internal method: Calculate system array
    def _calc_sysarray(self, bthres=0.1, athres=30.0, dthres=60.0):

        # Initialize array
        nrows = sum([len(ps.get_chmstr()) for ps in self.parscans])
        nmult = sum([len(self.dhmult[dt]) for dt in self.dhtypes])
        ncols = 3 * len(self.bdtypes) + 3 * len(self.antypes) + 2 * nmult
        npars = 2 * len(self.bdtypes) + 2 * len(self.antypes) + 2 * nmult
        self.sysarray = np.zeros((nrows, ncols))

        # Calculate arrays
        row = -1
        coltype = {}
        exccols = set([])
        for ps in self.parscans:
            bdfit = ps.select_bonds(bthres, bdrest=self.bdtypes)
            anfit = ps.select_angles(athres, anrest=self.antypes)
            dhfit = ps.select_diheds(dthres, dhrest=self.dhtypes)
            for cs in ps.get_chmstr():
                row += 1
                col = -1

                # Get bond values sorted as in self.bdtypes
                bdvals = OrderedDict([(bt, []) for bt in self.bdtypes])
                for bd, attr in cs.get_bonds_attr().items():
                    bt, bv = attr
                    bt = get_eqvtype(bt, self.bdeqv)
                    if bt.ismember(bdvals) and bd.ismember(bdfit):
                        bdvals[bt.getmember(bdvals)].append(bv)

                # Get angle values sorted as in self.antypes
                anvals = OrderedDict([(at, []) for at in self.antypes])
                for an, attr in cs.get_angles_attr().items():
                    at, av = attr
                    at = get_eqvtype(at, self.aneqv)
                    if at.ismember(anvals) and an.ismember(anfit):
                        anvals[at.getmember(anvals)].append(av)

                # Get dihedral values sorted as in self.dhtypes
                dhvals = OrderedDict([(dt, []) for dt in self.dhtypes])
                for dh, attr in cs.get_diheds_attr().items():
                    dt, dv = attr
                    dt = get_eqvtype(dt, self.dheqv)
                    if dt.ismember(dhvals) and dh.ismember(dhfit):
                        dhvals[dt.getmember(dhvals)].append(dv)

                # Calculate bond coeficients
                for bt, bvs in bdvals.items():
                    Ot = sum([1 for bv in bvs])
                    St = sum([bv for bv in bvs])
                    Qt = sum([bv*bv for bv in bvs])
                    col += 1
                    coltype[col] = bt
                    self.sysarray[row, col] = Ot
                    col += 1
                    coltype[col] = bt
                    self.sysarray[row, col] = (St / Ot if Ot != 0 else 0.0)
                    col += 1
                    exccols.add(col)
                    coltype[col] = bt
                    self.sysarray[row, col] = ((Qt*Ot - St**2) / Ot if Ot != 0 else 0.0)

                # Calculate angle coeficients
                for at, avs in anvals.items():
                    Ot = sum([1 for av in avs])
                    St = sum([av for av in avs])
                    Qt = sum([av*av for av in avs])
                    col += 1
                    coltype[col] = at
                    self.sysarray[row, col] = Ot
                    col += 1
                    coltype[col] = at
                    self.sysarray[row, col] = (St / Ot if Ot != 0 else 0.0)
                    col += 1
                    exccols.add(col)
                    coltype[col] = at
                    self.sysarray[row, col] = ((Qt*Ot - St**2) / Ot if Ot != 0 else 0.0)

                # Calculate dihed coeficients
                for dt, dvs in dhvals.items():
                    for m in self.dhmult[dt]:
                        col += 1
                        coltype[col] = dt
                        self.sysarray[row, col] = sum([math.cos(m * dv * math.pi / 180) for dv in dvs])
                        col += 1
                        exccols.add(col)
                        coltype[col] = dt
                        if self.dhflag[dt] == 'VAR':
                            self.sysarray[row, col] = sum([math.sin(m * dv * math.pi / 180) for dv in dvs])  

        # Check that array has no zero columns, except for exclude cols
        col = 0
        while (col < ncols) and (col in exccols or self.sysarray[:, col].any()):
            col += 1
        if col < ncols:
            partype = ' '.join(coltype[col].elems())
            if isinstance(coltype[col], BondType):
                parclass = 'bond'
                partol   = bthres
            elif isinstance(coltype[col], AngleType):
                parclass = 'angle'
                partol   = athres
            elif isinstance(coltype[col], DihedType):
                parclass = 'dihedral'
                partol   = dthres
            msg  = "\nError: zero coefficients in system array for parameter type " + partype 
            msg += "\nPossible causes:"
            msg += "\n  1. parameter type is not defined for any structure in the scans"
            msg += "\n  2. parameter is ignored (fluctuation in each scan < "
            msg += "{0:s} threshold = {1:.1f})\n".format(parclass, partol)
            msg += "\nCheck if " + partype + " is one of the scanned parameters or try lower threshold value to detect it\n"
            sys.exit(msg)


    # Internal method: Calculate initial guess according to array format
    def _calc_iguess(self, ipars={}, kb=200.0, b0=1.2, ka=50.0, a0=100.0, kchi=0.2, delta=0.0):

        # Initialize array
        nmult = sum([len(self.dhmult[dt]) for dt in self.dhtypes])
        npars = 2 * len(self.bdtypes) + 2 * len(self.antypes) + 2 * nmult
        iguess = np.zeros(npars)

        # Calculate initial guess for bonds
        par = -1
        for bt in self.bdtypes:
            par += 2
            kb_guess, b0_guess = (ipars[bt.getmember(ipars)] if bt.ismember(ipars) else (kb, b0))
            iguess[par-1], iguess[par] = kb_guess, b0_guess    

        # Calculate initial guess for angles
        for at in self.antypes:
            par += 2
            ka_guess, a0_guess = (ipars[at.getmember(ipars)] if at.ismember(ipars) else (ka, a0))
            iguess[par-1], iguess[par] = ka_guess, a0_guess   

        # Calculate initial guess for diheds
        for dt in self.dhtypes:
            idhlist = (ipars[dt.getmember(ipars)] if dt.ismember(ipars) else [])
            for m in self.dhmult[dt]:
                par += 1
                idhpars = {mi:(ki, di) for ki, mi, di in idhlist if mi == m}
                kchi_guess, delta_guess = (idhpars[m] if m in idhpars else (kchi, delta))
                iguess[par] = kchi_guess * math.cos(delta_guess * math.pi / 180)
                par += 1
                if self.dhflag[dt] == 'VAR':
                    iguess[par] = kchi_guess * math.sin(delta_guess * math.pi / 180)

        # Return initial guess
        return iguess

    
    # Internal method: get fit parameters from fit results
    def _get_fit_params(self, fitres, ffix=False):

        # Calculate bond parameters (kb, b0) and group by bond type
        j = -1
        bdpars = {}
        for bt in self.bdtypes:
            j += 2
            kb = fitres[j-1]
            b0 = fitres[j]
            bdpars[bt] = (kb, b0)

        # Calculate angle parameters (ka, a0) and group by angle type
        anpars = {}
        for at in self.antypes:
            j += 2
            ka = fitres[j-1]
            a0 = fitres[j]
            anpars[at] = (ka, a0)

        # Calculate dihedral parameters (kchi, mult, delta) and group by dihedral type
        dhpars = {}
        for dt in self.dhtypes:
            dp = []
            for mult in self.dhmult[dt]:
                j += 2
                if ffix or self.dhflag[dt] == 'FIX':
                    kchi  = math.fabs(fitres[j-1])                    
                    delta = math.atan2(0, fitres[j-1]) * 180 / math.pi
                else:
                    kchi  = math.hypot(fitres[j], fitres[j-1])                    
                    delta = math.atan2(fitres[j], fitres[j-1]) * 180 / math.pi
                dp.append((kchi, mult, delta))
            dhpars[dt] = dp
        return (bdpars, anpars, dhpars)      


    # Internal method: Energy difference
    def _ediff(self, pars, eref, ewgts, ffix=False):    
        col = 0
        par = 0 
        epar = 0
        for t in self.bdtypes:
            Ot, Bt, Ct = self.sysarray[:, col], self.sysarray[:, col+1], self.sysarray[:, col+2]
            kb, b0 = pars[par], pars[par+1]
            epar += kb * (Ot * (b0 - Bt)**2 + Ct)
            col  += 3
            par  += 2
        for t in self.antypes:
            Ot, At, Ct = self.sysarray[:, col], self.sysarray[:, col+1], self.sysarray[:, col+2]
            ka, a0 = pars[par], pars[par+1]
            epar += ka * (Ot * (a0 - At)**2 + Ct) * (math.pi / 180) ** 2
            col  += 3
            par  += 2
        for t in self.dhtypes:
            nc = 2 * len(self.dhmult[t])
            if ffix or self.dhflag[t] == 'FIX':
                epar += np.dot(self.sysarray[:, col:col+nc:2], pars[par:par+nc:2])
            else:
                epar += np.dot(self.sysarray[:, col:col+nc], pars[par:par+nc])
            col  += nc
            par  += nc
        return (epar - eref) * ewgts

    # Internal method: Square norm of energy difference
    def _sqediff(self, pars, eref, ewgts, ffix=False):    
        ediff = self._ediff(pars, eref, ewgts, ffix)
        return np.dot(ediff, ediff)


####################################################################################
##################################### METHODS ######################################
####################################################################################

# Get atoms from psf and pdb file
def get_atoms_from_file(psffile, pdbfile):

    # Read atom types from psf file        
    fpsf = open(psffile,'r')
    atomflag  = 0
    atomtypes = {}
    for line in fpsf:
        if re.search('!NATOM', line):
            atomflag = 1
        elif len(line.strip()) == 0:
            atomflag  = 0   
        elif atomflag == 1:
            cols = line.split()
            idx, typ = int(cols[0]), cols[5]
            atomtypes[idx] = typ  
    fpsf.close()

    # Read atom coords from pdb file
    fpdb = open(pdbfile,'r')
    atomcoords = {}
    for line in fpdb:
        if re.match('ATOM|HETATM', line):
            cols = line.split()
            idx  = int(cols[1])
            coords = tuple(float(c) for c in cols[5:8])
            atomcoords[idx] = coords
    fpdb.close()

    # Construct atom dictionary using atomtypes and atomcoords
    if set(atomtypes.keys()) != set(atomcoords.keys()):
        raise ValueError("Cannot build atoms dictionary! Atom types and atom coords must share exactly same indexes")
    return {idx:Atom(atomtypes[idx], atomcoords[idx]) for idx in atomtypes.keys()}

# Get bonds from psf file
def get_bonds_from_file(psffile):

    # Read bonds from psf file
    fpsf = open(psffile,'r')
    bondflag = 0
    bondtuples = []
    for line in fpsf:
        if re.search('!NBOND: bonds', line):
            bondflag = 1
        elif len(line.strip()) == 0:
            bondflag  = 0   
        elif bondflag == 1:
            cols = line.split()
            bondtuples.append(tuple(int(idx) for idx in cols[0:2]))
            bondtuples.append(tuple(int(idx) for idx in cols[2:4]))
            bondtuples.append(tuple(int(idx) for idx in cols[4:6]))
            bondtuples.append(tuple(int(idx) for idx in cols[6:8]))
    fpsf.close()
            
    # Construct bonds list from bonds tuples
    bondset = set([])
    for bd in bondtuples:
        if len(bd) == 2:
            bidx = BondIndex(bd)
            if not bidx.ismember(bondset):             
                bondset.add(bidx)
    return sorted(bondset)

# Get angles from psf file
def get_angles_from_file(psffile):

    # Read angles from psf file
    fpsf = open(psffile,'r')
    angleflag = 0
    angletuples = []
    for line in fpsf:
        if re.search('!NTHETA: angles', line):
            angleflag = 1
        elif len(line.strip()) == 0:
            angleflag  = 0   
        elif angleflag == 1:
            cols = line.split()
            angletuples.append(tuple(int(idx) for idx in cols[0:3]))
            angletuples.append(tuple(int(idx) for idx in cols[3:6]))
            angletuples.append(tuple(int(idx) for idx in cols[6:9]))
    fpsf.close()
            
    # Construct angle list from angle tuples
    angleset = set([])
    for an in angletuples:
        if len(an) == 3:
            aidx = AngleIndex(an)
            if not aidx.ismember(angleset):             
                angleset.add(aidx)
    return sorted(angleset)

# Get dihedrals from psf file
def get_diheds_from_file(psffile):

    # Read dihedrals from psf file
    fpsf = open(psffile,'r')
    dihedflag = 0
    dihedtuples = []
    for line in fpsf:
        if re.search('!NPHI: dihedrals', line):
            dihedflag = 1
        elif len(line.strip()) == 0:
            dihedflag  = 0   
        elif dihedflag == 1:
            cols = line.split()
            dihedtuples.append(tuple(int(idx) for idx in cols[0:4]))
            dihedtuples.append(tuple(int(idx) for idx in cols[4:8]))
    fpsf.close()
            
    # Construct dihedrals list from dihedrals tuples
    dihedset = set([])
    for dh in dihedtuples:
        if len(dh) == 4:
            didx = DihedIndex(dh)
            if not didx.ismember(dihedset):             
                dihedset.add(didx)
    return sorted(dihedset)


# Get equivalent type
def get_eqvtype(ptype, eqvtypes):
    if ptype.ismember(eqvtypes):
        etype = eqvtypes[ptype.getmember(eqvtypes)]
    else:
        etype = ptype
    return etype



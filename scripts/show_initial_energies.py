#!/usr/bin/python

import os
import re
import sys
import math
import string
import warnings
import numpy as np
from itertools import izip
from matplotlib import pyplot as plt, rcParams

rcParams['font.size'] = 14
warnings.filterwarnings("ignore",".*GUI is implemented.*")


###############################################################################################################
################################################## MAIN PROGRAM ###############################################
###############################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = qm energy file
# sys.argv[2] = mm energy file

# Show usage
if len(sys.argv) != 4:
    msg  = '\nUsage: show_initial_energy.py [qm_file] [mm_file] [show_unconv]\n'
    sys.exit(msg)

# Input variables
qmefile = sys.argv[1]
mmefile = sys.argv[2]
show_unconv = (sys.argv[3] == 'YES')

# Get qm energy data, convert from Hartree to Kcal/mol (qconv = 627.503)
qconv    = 627.503
finp     = open(qmefile, 'r')
qmdata   = [ln.split() for ln in finp if not ln.strip().startswith('#')]
qmdata   = [data for data in qmdata if data[-1] != 'OPT_UNCONV' or show_unconv]
qmener   = [(float(pval), float(ener) * qconv) for pdb, pval, ener, stat in qmdata]
qmemin   = min([qme for pval, qme in qmener])
qmener   = sorted([(pval, qme - qmemin) for pval, qme in qmener], key=lambda x: x[0])
pvdict   = {pdb:float(pval) for pdb, pval, ener, stat in qmdata}
enerlist = [qmener]
lablist  = ['QM']
finp.close()

# Get mm energy data
finp = open(mmefile, 'r')
mmdata   = [ln.split() for ln in finp if not ln.strip().startswith('#')]
mmener   = [(pvdict[pdb], float(ener)) for pdb, ener in mmdata if pdb in pvdict]
mmemin   = min([mme for pval, mme in mmener])
mmener   = sorted([(pval, mme - mmemin) for pval, mme in mmener], key=lambda x: x[0])
enerlist.append(mmener)
lablist.append('MM_initial')
finp.close()

# Plot energies
i = -1
cls = {0:'r', 1:'b', 2:'g', 3:'y', 4:'c', 5:'m'}
mks = {0:'s', 1:'D', 2:'o', 3:'^', 4:'*', 5:'x'}
fig, ax = plt.subplots(figsize=(9,7))
ptitle = qmefile.strip('/').split('/')[-2]
for enerdata in enerlist:
    i += 1
    pval, ener = izip(*enerdata)
    ax.plot(pval, ener, color=cls[i%6], marker=mks[i%6], ms=8, linewidth=2.0, label=lablist[i])
ax.set_xlabel('Scan value', labelpad=10)
ax.set_ylabel('Energy (kcal/mol)', labelpad=10)
ax.legend(fontsize=13, framealpha=0.8)
ax.set_title(ptitle, fontdict={'va':'bottom', 'fontsize':15})
ax.margins(0.05)
ax.grid()
plt.tight_layout()
plt.ion()
plt.show()
plt.pause(2.0) 


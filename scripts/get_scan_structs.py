#!/usr/bin/python

import re
import os
import sys
import string
from itertools import izip
from cclib.io import ccopen
from cclib.parser.utils import convertor, PeriodicTable


#################################################################################################
########################################## FUNCTIONS ############################################
#################################################################################################

# Get structures from log files in input dir
def get_structs_cclib(inpdir):
    sstructs  = []
    osdict    = {}
    osdict[0] = 'OPT_UNK'
    osdict[1] = 'OPT_NEW'
    osdict[2] = 'OPT_DONE'
    osdict[3] = 'OPT_UNCONV'
    ptable = PeriodicTable()
    logs = sorted([os.path.join(inpdir, f) for f in os.listdir(inpdir) if f.endswith(".log")])
    for log in logs:
        sdata  = ccopen(log, optdone_as_list=True).parse()
        natoms = sdata.natom
        atypes = [ptable.element[n] for n in sdata.atomnos]
        acoord = sdata.atomcoords
        energy = sdata.scfenergies
        if 'optdone' not in sdata.getattributes():
            sstructs += [(natoms, atypes, crd, ener, 'SP_ENERGY') for crd, ener in izip(acoord, energy)]
        else:
            soptdone  = sdata.optdone
            soptstat  = [osdict[ostat] for ostat in sdata.optstatus]
            sstructs += [(natoms, atypes, acoord[i], energy[i], soptstat[i]) for i in soptdone]
    return sstructs 
    

# Write data
def write_structs_data(sstructs, outdir):
    ns = -1
    qmfile = outdir + '/' + 'qm_energies.dat'
    if not os.path.isfile(qmfile):
        nv = -1
        fqme = open(outdir + '/' + 'qm_energies.dat', 'w')
        fqme.write("# PDB".ljust(8))
        fqme.write("QM_Energy".rjust(18))
        fqme.write("Opt_Status".rjust(16) + "\n")
        fqme.write("# File".ljust(8))
        fqme.write("(Hartree)".rjust(18) + "\n")
    else:
        fqme = open(outdir + '/' + 'qm_energies.dat', 'a')
    for natoms, atypes, acoord, energy, ostat in sstructs:
        ns += 1 
        xyzname = '{:03d}.xyz'.format(ns)
        fqme.write(xyzname.ljust(8))
        fqme.write("{:6.8f}".format(convertor(energy, 'eV', 'hartree')).rjust(19)) 
        fqme.write(ostat.rjust(15) + "\n")
        fxyz = open(outdir + '/' + xyzname, 'w')
        fxyz.write(str(natoms) + '\n')
        fxyz.write(xyzname + '\n')
        rows, cols = acoord.shape
        for i in range(rows):
            fxyz.write(atypes[i].ljust(5))
            for j in range(cols):
                fxyz.write('{:.6f}'.format(acoord[i,j]).rjust(10) + '  ')
            fxyz.write('\n')
        fxyz.close()
    fqme.close()

        
##################################################################################################
########################################### MAIN PROGRAM #########################################
##################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input logs dir 
# sys.argv[2] = output struct dir 

# Usage
if len(sys.argv) != 3:
    msg = "\nUsage: get_scan_structs.py [input_log] [output_dir]\n"
    sys.exit(msg) 

# Get data and write results
sstructs = get_structs_cclib(sys.argv[1])
write_structs_data(sstructs, sys.argv[2])


#!/usr/bin/python

import os
import re
import sys
import string
from itertools import izip
from paramtools import *
  
        
############################################################################################################
############################################### MAIN PROGRAM ###############################################
############################################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = psf file
# sys.argv[2]  = mm energy file for initial parameters
# sys.argv[3]  = mm energy file for new parameters
# sys.argv[4]  = initial parameters file
# sys.argv[5]  = new parameters file

# Usage
if len(sys.argv) != 6:
    msg = "\nUsage: get_energies_new.py [psffile] [mme_init] [mme_new] [prm_init] [prm_new]\n"
    sys.exit(msg) 

# Input args
psffile  = sys.argv[1]
mmifile  = sys.argv[2]
mmnfile  = sys.argv[3]
prmifile = sys.argv[4]
prmnfile = sys.argv[5]

# Patterns
intval    = '-?[0-9]+'
fltval    = '-?[0-9]+\.[0-9]*'
bondvals  = fltval + '\s+' + fltval + '\s*[^0-9]' 
anglevals = fltval + '\s+' + fltval + '\s*[^0-9]'
dihedvals = fltval + '\s+' + intval + '\s+' + fltval + '\s*[^0-9]'
bondtype  = '^([A-Za-z][A-Za-z0-9]+\s+){1,1}[A-Za-z][A-Za-z0-9]+'
angletype = '^([A-Za-z][A-Za-z0-9]+\s+){2,2}[A-Za-z][A-Za-z0-9]+'
dihedtype = '^([A-Za-z][A-Za-z0-9]+\s+){3,3}[A-Za-z][A-Za-z0-9]+'
bondpatt  = bondtype  + '\s+' + bondvals[:-9]  + '$'
anglepatt = angletype + '\s+' + anglevals[:-9] + '$'
dihedpatt = dihedtype + '\s+' + dihedvals[:-9] + '$'

# Load new parameters file
fprmn = open(prmnfile, 'r')
prmnlist = [line.strip() for line in fprmn if not line.lstrip().startswith('#')]
prmnlist = [par for par in prmnlist if len(par) > 0]
fprmn.close()

# Separate bonds, angles, diheds
prmnbonds = {}
prmnangles = {}
prmndiheds = {}
newbonds  = [par for par in prmnlist if re.match(bondpatt, par)]
newangles = [par for par in prmnlist if re.match(anglepatt, par)]
newdiheds = [par for par in prmnlist if re.match(dihedpatt, par)]
noassign  = [par for par in prmnlist if par not in newbonds + newangles + newdiheds]
if len(noassign) > 0:
    msg  = "\nError: Cannot set new parameters. "
    msg += "Invalid format for parameter " + noassign[0] + '\n'
    sys.exit(msg)
else:

    # Add new bonds
    for par in newbonds:
        ps = par.split()
        bond, kb, r0 = BondType(ps[:2]), float(ps[2]), float(ps[3])
        if bond.ismember(prmnbonds):
            prmnbonds[bond.getmember(prmnbonds)] = (kb, r0)
        else:
            prmnbonds[bond] = (kb, r0)

    # Add new angles
    for par in newangles:
        ps = par.split()
        angle, ka, th = AngleType(ps[:3]), float(ps[3]), float(ps[4])
        if angle.ismember(prmnangles):
            prmnangles[angle.getmember(prmnangles)] = (ka, th)
        else:
            prmnangles[angle] = (ka, th)

    # Add new diheds
    for par in newdiheds:
        ps = par.split()
        dihed, kchi, mult, delta = DihedType(ps[:4]), float(ps[4]), int(ps[5]), float(ps[6])
        if not dihed.ismember(prmndiheds):
            prmndiheds[dihed] = {}
        prmndiheds[dihed.getmember(prmndiheds)][mult] = (kchi, delta)

# Load initial parameters file
prmilines = []
fprmi = open(prmifile, 'r')
for line in fprmi:
    prmilines.append(line.strip())
fprmi.close()

# Load initial parameters when matching new parameters
prmibonds  = {}
prmiangles = {}
prmidiheds = {}
newbondtypes  = prmnbonds.keys()
newangletypes = prmnangles.keys()
newdihedtypes = prmndiheds.keys()
for line in prmilines:
    found = False

    # Bonds
    i = 0
    while i < len(newbondtypes) and not found:
        bond   = newbondtypes[i]
        rbond  = bond.reverse()
        bpatt1 = '\s*' + '\s+'.join(bond.elems())  + '\s+' + bondvals
        bpatt2 = '\s*' + '\s+'.join(rbond.elems()) + '\s+' + bondvals
        if re.match(bpatt1, line) or re.match(bpatt2, line):
            kb, r0 = map(float, re.findall(fltval, line)[:2])
            if bond.ismember(prmibonds):
                prmibonds[bond.getmember(prmibonds)] = (kb, r0)
            else:
                prmibonds[bond] = (kb, r0)
            found = True
        i += 1

    # Angles
    i = 0
    while i < len(newangletypes) and not found:
        angle  = newangletypes[i]
        rangle = angle.reverse()
        apatt1 = '\s*' + '\s+'.join(angle.elems())  + '\s+' + anglevals
        apatt2 = '\s*' + '\s+'.join(rangle.elems()) + '\s+' + anglevals
        if re.match(apatt1, line) or re.match(apatt2, line):
            ka, th = map(float, line.split()[3:5])
            if angle.ismember(prmiangles):
                prmiangles[angle.getmember(prmiangles)] = (ka, th)
            else:
                prmiangles[angle] = (ka, th)
            found = True
        i += 1

    # Diheds
    i = 0
    while i < len(newdihedtypes) and not found:
        dihed  = newdihedtypes[i]
        rdihed = dihed.reverse()
        dpatt1 = '\s*' + '\s+'.join(dihed.elems())  + '\s+' + dihedvals
        dpatt2 = '\s*' + '\s+'.join(rdihed.elems()) + '\s+' + dihedvals
        if re.match(dpatt1, line) or re.match(dpatt2, line):
            kchi, mult, delta = line.split()[4:7]
            kchi, mult, delta = float(kchi), int(mult), float(delta)
            if not dihed.ismember(prmidiheds):
                prmidiheds[dihed] = {}
            prmidiheds[dihed.getmember(prmidiheds)][mult] = (kchi, delta)
            found = True
        i += 1


# Load initial energy file
fmmi = open(mmifile, 'r')
mmilist = [line.split() for line in fmmi if not line.lstrip().startswith('#')]
fmmi.close() 

# Get structures from pdb files and calculate mm energy for new parameters
# NOTE: pdb files are assumed to be in the same directory as psf file
mmnlist = []
basedir  = os.path.dirname(psffile)
for pdb, mmi in mmilist:
    pdbfile   = basedir + '/' + pdb
    atomlist  = get_atoms_from_file(psffile, pdbfile)
    bondlist  = get_bonds_from_file(psffile)
    anglelist = get_angles_from_file(psffile)
    dihedlist = get_diheds_from_file(psffile)
    chmstruct = ChemStruct(atomlist, bondlist, anglelist, dihedlist)
    prmiener  = chmstruct.get_bonds_energy(prmibonds)
    prmiener += chmstruct.get_angles_energy(prmiangles)
    prmiener += chmstruct.get_diheds_energy(prmidiheds)
    prmnener  = chmstruct.get_bonds_energy(prmnbonds)
    prmnener += chmstruct.get_angles_energy(prmnangles)
    prmnener += chmstruct.get_diheds_energy(prmndiheds)
    mmn = float(mmi) - prmiener + prmnener
    mmnlist.append((pdb, mmn))
    
# Write new energy file
fmmn = open(mmnfile, 'w')
fmmn.write('# PDB'.ljust(14)  + ' ' + 'MM_Energy'.ljust(9) + '\n')
fmmn.write('# File'.ljust(14) + ' ' + '(Kcal/mol)'.ljust(9) + '\n')
for pdb, mmn in mmnlist:
    fmmn.write(pdb.ljust(14) + ' ' + '{:.6f}'.format(mmn).rjust(9) + '\n')
fmmn.close() 




#!/usr/bin/python

import os
import re
import sys
import string
from itertools import izip
from paramtools import *
  
        
################################################################################################################
################################################## MAIN PROGRAM ################################################
################################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = input file defining necessary arguments to load structures info (psf, pdb, qme, mme)
# sys.argv[2] = input file containing the list of parameter types to be fitted
# sys.argv[3] = output file reporting the resulting fitted parameters
# sys.argv[4] = threshold for fit bonds
# sys.argv[5] = threshold for fit angles
# sys.argv[6] = threshold for fit dihedrals
# sys.argv[7] = use unconverged structures 

# Usage
if len(sys.argv) != 8:
    msg = "\nUsage: fit_scan_params.py [argfile] [parfile] [outfile] [bthres] [athres] [dthres] [use_unconv]\n"
    sys.exit(msg) 

# Input args
argfile = sys.argv[1]
parfile = sys.argv[2]
outfile = sys.argv[3]
bthres = float(sys.argv[4])
athres = float(sys.argv[5])
dthres = float(sys.argv[6])
use_unconv = (sys.argv[7] == 'YES')

# Read arguments to load structure data
farg = open(argfile, 'r')
arglist = [line.split() for line in farg if len(line.strip()) > 0]
farg.close()

# Patterns
bondtype  = '^([A-Za-z][A-Za-z0-9]+\s+){1,1}[A-Za-z][A-Za-z0-9]+'
angletype = '^([A-Za-z][A-Za-z0-9]+\s+){2,2}[A-Za-z][A-Za-z0-9]+'
dihedtype = '^([A-Za-z][A-Za-z0-9]+\s+){3,3}[A-Za-z][A-Za-z0-9]+'
dihedmult = '(\s*,\s*([0-9]+\s+)+[0-9]+)?'
dihedflag = '(\s*,\s*(FIX|VAR))?'
bondpatt  = bondtype  + '$'
anglepatt = angletype + '$'
dihedpatt = dihedtype + dihedmult + dihedflag + '\s*$'

# Read parameters to be fitted
fpar = open(parfile, 'r')
parlist = [line.strip() for line in fpar if not line.lstrip().startswith('#')]
parlist = [par for par in parlist if len(par) > 0]
fpar.close()

# Separate bonds, angles, diheds
bondlist  = [par for par in parlist if re.match(bondpatt, par)]
anglelist = [par for par in parlist if re.match(anglepatt, par)]
dihedlist = [par for par in parlist if re.match(dihedpatt, par)]
noassign  = [par for par in parlist if par not in bondlist + anglelist + dihedlist]
if len(noassign) > 0:
    msg = "\nError: Invalid format for parameter values: " + noassign[0] + '\n'
    sys.exit(msg)
else:
    bondlist  = [par.split() for par in bondlist]
    anglelist = [par.split() for par in anglelist]
    dihedlist = [par.split(',') for par in dihedlist]

# Set bonds, angles and dihedral types
fitbonds  = [BondType(par)  for par in bondlist]
fitangles = [AngleType(par) for par in anglelist]
fitdiheds = [DihedType(par[0].split()) for par in dihedlist]
fitbonds  = set([min(bd, bd.reverse()) for bd in fitbonds])
fitangles = set([min(an, an.reverse()) for an in fitangles])
fitdiheds = set([min(dh, dh.reverse()) for dh in fitdiheds])

# Set dihedrals multiplicities and flags
fitdmult = {}
fitdflag = {}
defmult  = range(1, 5)
defflag  = 'FIX'
for par in dihedlist:  
    dh = DihedType(par[0].split())
    dh = min(dh, dh.reverse())
    if len(par) == 1:
        mult = defmult
        flag = defflag
    elif (len(par) == 2) and par[1].replace(' ', '').isdigit():
        mult = map(int, par[1].split())
        flag = defflag
    elif (len(par) == 2) and re.search('FIX|VAR', par[1]):
        mult = defmult
        flag = par[1].strip()
    else:
        mult = map(int, par[1].split())
        flag = par[2].strip() 
    fitdmult[dh] = mult
    fitdflag[dh] = flag
          
# Get psf, pdb and energy files for each scan and generate the list of scans
# QM and MM energies are assumed to be in Hartree and Kcal/mol respectively 
basedir = ''
chmlist = []
parscans = []
for args in arglist:
    if len(args) == 1:
        if len(chmlist) > 0:
            parscans.append(ParamScan(chmlist))
        chmlist = []
        basedir = args[0] 
    elif (len(args) == 5) and (len(basedir) > 0):
        psf, pdb, qme, stat, mme = args
        psffile = basedir + '/' + psf
        pdbfile = basedir + '/' + pdb
        if os.path.isfile(pdbfile) and (stat != 'OPT_UNCONV' or use_unconv):
            atomlist  = get_atoms_from_file(psffile, pdbfile)
            bondlist  = get_bonds_from_file(psffile)
            anglelist = get_angles_from_file(psffile)
            dihedlist = get_diheds_from_file(psffile)
            chmlist.append(ChemStruct(atoms=atomlist, bonds=bondlist, 
                                      angles=anglelist, diheds=dihedlist,
                                      qme=float(qme), mme=float(mme)))
if len(chmlist) > 0:
    parscans.append(ParamScan(chmlist))  

# Fit parameters
parfit = ParamFit(parscans, bdtypes=fitbonds, antypes=fitangles, 
                            dhtypes=fitdiheds, dhmult=fitdmult, dhflag=fitdflag)
bdpars, anpars, dhpars = parfit.fit(bthres=bthres, athres=athres, dthres=dthres)

# Write parameters
fout = open(outfile, 'w')

fout.write('# Bonds' + '\n')
for btyp, bvals in bdpars.items():
    bstr = ' '.join(btyp.elems())
    kb, b0 = bvals
    fout.write("{0:18s} {1:>9.2f} {2:>9.4f}".format(bstr, kb, b0) + '\n')
fout.write('\n')

fout.write('# Angles' + '\n')
for atyp, avals in anpars.items():
    astr = ' '.join(atyp.elems())
    ka, a0 = avals
    fout.write("{0:18s} {1:>9.2f} {2:>9.2f}".format(astr, ka, a0) + '\n')
fout.write('\n')

fout.write('# Dihedrals' + '\n')
for dtyp, dvals in dhpars.items():
    dstr = ' '.join(dtyp.elems())
    for kchi, mult, delta in dvals:
        fout.write("{0:30s} {1:>9.3f} {2:>4d} {3:>9.2f}".format(dstr, kchi, mult, delta) + '\n')
    fout.write('\n')
fout.write('\n')
fout.close()


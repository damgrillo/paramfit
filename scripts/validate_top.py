#!/usr/bin/python

import re
import sys
import string
from itertools import izip

       
####################################################################################################
############################################### MAIN PROGRAM #######################################
####################################################################################################

# Command line arguments
# sys.argv[0] = program itself
# sys.argv[1] = top file
# sys.argv[2] = pdb file

# Usage
if len(sys.argv) != 3:
    msg = "\nUsage: validate_top.py [input_top] [input_pdb]\n"
    sys.stderr.write(msg)
    sys.exit() 

# Read topology
topresid = ''
topatoms = []
topfile  = open(sys.argv[1], 'r')
for line in topfile:
    if re.match('RESI\s+', line):
        topresid = line.split()[1]
    elif re.match('ATOM\s+', line):
        topatoms.append(line.split()[2])
topfile.close()

# Read pdb file and validate topology 
i = 0 
topvalid = True
pdbfile  = open(sys.argv[2], 'r')
pdblines = [line for line in pdbfile]
pdbfile.close()
while topvalid and i < len(pdblines):
    line = pdblines[i]
    if re.match('ATOM\s+|HETATM\s+', line):
        rtyp, anum, aname, resid, resnum, xc, yc, zc, occ, tf, asym  = line.split()[:11]
        pdb_atom = string.upper(asym)
        top_atom = string.upper(topatoms[int(anum)-1][:len(asym)])
        if pdb_atom != top_atom:
            topvalid = False
    i += 1

# Print message
if topvalid:
    print "Valid topology: " + topresid
else:
    print "Valid topology: Error!"



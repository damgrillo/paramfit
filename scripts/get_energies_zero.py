#!/usr/bin/python

import os
import re
import sys
import string
from itertools import izip
from paramtools import *
  
        
############################################################################################################
############################################### MAIN PROGRAM ###############################################
############################################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = psf file
# sys.argv[2]  = mm energy file for initial parameters
# sys.argv[3]  = mm energy file for fit parameters on zero
# sys.argv[4]  = initial parameters file
# sys.argv[5]  = list of zero parameters, comma separated

# Usage
if len(sys.argv) != 6:
    msg = "\nUsage: get_energies_zero.py [psffile] [mme_init] [mme_zero] [prm_init] [prm_zero]\n"
    sys.exit(msg) 

# Input args
psffile  = sys.argv[1]
mmifile  = sys.argv[2]
mmzfile  = sys.argv[3]
prmifile = sys.argv[4]
prmzlist = sys.argv[5]

# Patterns
intval    = '-?[0-9]+'
fltval    = '-?[0-9]+\.[0-9]*'
bondvals  = fltval + '\s+' + fltval + '\s*[^0-9]' 
anglevals = fltval + '\s+' + fltval + '\s*[^0-9]'
dihedvals = fltval + '\s+' + intval + '\s+' + fltval + '\s*[^0-9]'
bondtype  = '^([A-Za-z][A-Za-z0-9]+\s+){1,1}[A-Za-z][A-Za-z0-9]+$'
angletype = '^([A-Za-z][A-Za-z0-9]+\s+){2,2}[A-Za-z][A-Za-z0-9]+$'
dihedtype = '^([A-Za-z][A-Za-z0-9]+\s+){3,3}[A-Za-z][A-Za-z0-9]+$'

# Load zero parameters
prmzlist   = [par.strip() for par in prmzlist.split(',') if len(par.strip()) > 0]
zerobonds  = [par for par in prmzlist if re.match(bondtype, par)]
zeroangles = [par for par in prmzlist if re.match(angletype, par)]
zerodiheds = [par for par in prmzlist if re.match(dihedtype, par)]
noassign   = [par for par in prmzlist if par not in zerobonds + zeroangles + zerodiheds]
if len(noassign) > 0:
    msg  = "\nError: Cannot read parameters. "
    msg += "Invalid format for parameter " + noassign[0] + '\n'
    sys.exit(msg)
else:
    zerobonds  = [BondType(par.split()) for par in zerobonds]
    zeroangles = [AngleType(par.split()) for par in zeroangles]
    zerodiheds = [DihedType(par.split()) for par in zerodiheds]
    zerobonds  = [min(bd, bd.reverse()) for bd in zerobonds]
    zeroangles = [min(an, an.reverse()) for an in zeroangles]
    zerodiheds = [min(dh, dh.reverse()) for dh in zerodiheds]

# Load initial parameters file
prmilines = []
fprmi = open(prmifile, 'r')
for line in fprmi:
    prmilines.append(line.strip())
fprmi.close()

# Load initial parameters when matching zero parameters
prmibonds  = {}
prmiangles = {}
prmidiheds = {}
for line in prmilines:
    found = False

    # Bonds
    i = 0
    while i < len(zerobonds) and not found:
        bond   = zerobonds[i]
        rbond  = bond.reverse()
        bpatt1 = '\s*' + '\s+'.join(bond.elems())  + '\s+' + bondvals
        bpatt2 = '\s*' + '\s+'.join(rbond.elems()) + '\s+' + bondvals
        if re.match(bpatt1, line) or re.match(bpatt2, line):
            kb, r0 = map(float, re.findall(fltval, line)[:2])
            if bond.ismember(prmibonds):
                prmibonds[bond.getmember(prmibonds)] = (kb, r0)
            else:
                prmibonds[bond] = (kb, r0)
            found = True
        i += 1

    # Angles
    i = 0
    while i < len(zeroangles) and not found:
        angle  = zeroangles[i]
        rangle = angle.reverse()
        apatt1 = '\s*' + '\s+'.join(angle.elems())  + '\s+' + anglevals
        apatt2 = '\s*' + '\s+'.join(rangle.elems()) + '\s+' + anglevals
        if re.match(apatt1, line) or re.match(apatt2, line):
            ka, th = map(float, line.split()[3:5])
            if angle.ismember(prmiangles):
                prmiangles[angle.getmember(prmiangles)] = (ka, th)
            else:
                prmiangles[angle] = (ka, th)
            found = True
        i += 1

    # Diheds
    i = 0
    while i < len(zerodiheds) and not found:
        dihed  = zerodiheds[i]
        rdihed = dihed.reverse()
        dpatt1 = '\s*' + '\s+'.join(dihed.elems())  + '\s+' + dihedvals
        dpatt2 = '\s*' + '\s+'.join(rdihed.elems()) + '\s+' + dihedvals
        if re.match(dpatt1, line) or re.match(dpatt2, line):
            kchi, mult, delta = line.split()[4:7]
            kchi, mult, delta = float(kchi), int(mult), float(delta)
            if not dihed.ismember(prmidiheds):
                prmidiheds[dihed] = {}
            prmidiheds[dihed.getmember(prmidiheds)][mult] = (kchi, delta)
            found = True
        i += 1

# Load initial energy file
fmmi = open(mmifile, 'r')
mmilist = [line.split() for line in fmmi if not line.lstrip().startswith('#')]
fmmi.close() 

# Get structures from pdb files and calculate mm energy for zero parameters
# NOTE: pdb files are assumed to be in the same directory as psf file
mmzlist = []
basedir  = os.path.dirname(psffile)
for pdb, mmi in mmilist:
    pdbfile   = basedir + '/' + pdb
    atomlist  = get_atoms_from_file(psffile, pdbfile)
    bondlist  = get_bonds_from_file(psffile)
    anglelist = get_angles_from_file(psffile)
    dihedlist = get_diheds_from_file(psffile)
    chmstruct = ChemStruct(atomlist, bondlist, anglelist, dihedlist)
    prmiener  = chmstruct.get_bonds_energy(prmibonds)
    prmiener += chmstruct.get_angles_energy(prmiangles)
    prmiener += chmstruct.get_diheds_energy(prmidiheds)
    mmz = float(mmi) - prmiener
    mmzlist.append((pdb, mmz))
    
# Write zero energy file
fmmz = open(mmzfile, 'w')
fmmz.write('# PDB'.ljust(14)  + ' ' + 'MM_Energy'.ljust(9) + '\n')
fmmz.write('# File'.ljust(14) + ' ' + '(Kcal/mol)'.ljust(9) + '\n')
for pdb, mmz in mmzlist:
    fmmz.write(pdb.ljust(14) + ' ' + '{:.6f}'.format(mmz).rjust(9) + '\n')
fmmz.close() 




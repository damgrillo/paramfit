#!/usr/bin/python

import re
import sys
import string
from itertools import izip
from paramtools import *

        
##################################################################################################
######################################### MAIN PROGRAM ###########################################
##################################################################################################

# Command line arguments
# sys.argv[0]  = program itself
# sys.argv[1]  = psf file
# sys.argv[2+] = pdb file list

# Usage
if len(sys.argv) < 7:
    msg =  "\nUsage: detect_scan_params.py [bthres] [athres] [dthres] [qme_file] [psf_file] [pdb_file_list]\n"
    sys.exit(msg)

# Input files
bthres = float(sys.argv[1])
athres = float(sys.argv[2])
dthres = float(sys.argv[3])
qmefile = sys.argv[4]
psffile = sys.argv[5]
pdblist = sys.argv[6:]

# Read energies for each pdb
fqme = open(qmefile, 'r')
qmlines  = [line for line in fqme if not line.strip().startswith('#')]
qmlines  = [line.split() for line in qmlines if len(line.split()) == 3]
enerdict = {pdb:ener for pdb, ener, stat in qmlines}
statdict = {pdb:stat for pdb, ener, stat in qmlines}
fqme.close()

# Skip procedure when qm file does not require editing
if len(qmlines) == 0:
    msg = "Skipping procedure, qm energy file does not require editing"
    sys.exit(msg)

# Get structure info from input files
chmlist = []
pdbnames = []
for pdbfile in pdblist:
    bondlist  = get_bonds_from_file(psffile)
    anglelist = get_angles_from_file(psffile)
    dihedlist = get_diheds_from_file(psffile)
    atomlist  = get_atoms_from_file(psffile, pdbfile)
    chmlist  += [ChemStruct(atomlist, bondlist, anglelist, dihedlist)]
    pdbnames += [pdbfile.rstrip('/').split('/')[-1]]

# Select scan parameterbased on max variability
pscan   = ParamScan(chmlist)
bdscan  = pscan.select_bonds(bthres)
anscan  = pscan.select_angles(athres)
dhscan  = pscan.select_diheds(dthres)
pselect = []
if len(bdscan) > 0:
    paridx = max(bdscan.items(), key=lambda x: x[1][1])[0]
    partyp, parvar = bdscan[paridx]
    pselect.append((paridx, partyp, parvar))
if len(anscan) > 0:
    paridx = max(anscan.items(), key=lambda x: x[1][1])[0]
    partyp, parvar = anscan[paridx]
    pselect.append((paridx, partyp, parvar))
if len(dhscan) > 0:
    paridx = max(dhscan.items(), key=lambda x: x[1][1])[0]
    partyp, parvar = dhscan[paridx]
    pselect.append((paridx, partyp, parvar))
if len(pselect) == 0: 
    msg = "Error: No scan parameter found, try lower threshold values"
    sys.exit(msg)
elif len(pselect) > 1:
    msg  = "Error: More than one scan parameter found, adjust threshold values\n"
    maxlen = max([len('_'.join(partyp.elems())) for paridx, partyp, parvar in pselect])
    for paridx, partyp, parvar in pselect:
        msg += "Scan parameter:  "
        msg += ' '.join(partyp.elems()).ljust(maxlen)
        msg += '  ->  Max variability:  {0:>7.3f}'.format(parvar)
        if isinstance(paridx, BondIndex):
            msg += '  (Current bond threshold  = {0:>6.3f})'.format(bthres) + '\n'
        elif isinstance(paridx, AngleIndex):
            msg += '  (Current angle threshold = {0:>6.1f})'.format(athres)  + '\n'
        else:
            msg += '  (Current dihed threshold = {0:>6.1f})'.format(dthres)  + '\n'
    sys.exit(msg[:-1])
else:
    partyp = pselect[0][1]
    msg  = "Scan parameter detected:  "
    msg += ' '.join(partyp.elems())
    print msg
    
# Get values for scan parameter 
parlist = []
paridx, partyp = pselect[0][0:2]
for chm in chmlist:
    if isinstance(paridx, BondIndex):
        pardict = chm.get_bonds_attr()
    elif isinstance(paridx, AngleIndex):
        pardict = chm.get_angles_attr()
    else:
        pardict = chm.get_diheds_attr()
    parlist.append(pardict[paridx.getmember(pardict)][1])
parlist = [(par - math.floor((par + 180) / 360) * 360) for par in parlist]

# Get units
if isinstance(partyp, BondIndex):
    parunits = '(angstrom)'
else:
    parunits = '(degrees)'

# Write scan parameter value and energy for each pdb 
print "Updating qm_energies.dat ..."
fqme = open(qmefile, 'w')
sout  = "# PDB".ljust(8)
sout += "Scan_Value".rjust(14)
sout += "QM_Energy".rjust(18)
sout += "Opt_Status".rjust(16) + "\n"
sout += "# File".ljust(8)
sout += parunits.rjust(14)
sout += "(Hartree)".rjust(18)  + "\n"
fqme.write(sout)
for pdb, par in izip(pdbnames, parlist):
        sout  = pdb.ljust(8)
        sout += '{:.6f}'.format(par).rjust(14)
        sout += enerdict[pdb].rjust(19) 
        sout += statdict[pdb].rjust(15) + "\n"
        fqme.write(sout)
fqme.close()


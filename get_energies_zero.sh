#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: get_energies_zero.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir
         -o  Output dir
         -p  Zero parameters file
         -f  Force Field dir

DESCRIPTION:

         - The program gets the MM energies setting provided parameters on zero for each PDB structure
           in the input dir and saves energies in the output dir.

         - The MM energy file with zero parameters (mm_energies_zero.dat) will be in tabular
           format, in the first field the names of the PDB files, in the second field the 
           MM energies in Kcal/mol.  

         - All the PDB structures in the input dir are supposed to have the same 
           topology, which must be defined in the PSF file inside the same dir.\n"
}

# Options parser
while getopts "i:o:p:f:" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        o)
            OUTDIR=$OPTARG;;
        p)
            ZPARFILE=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" || -z "$OUTDIR" || -z "$ZPARFILE" || -z "$FFDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
elif [[ ! -d "$OUTDIR" ]]; then
    echo "Error: $OUTDIR is not a directory"; exit 1
elif [[ ! -f "$ZPARFILE" ]]; then
    echo "Error: $ZPARFILE is not a file"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi

# Export path to extra python modules
export PYTHONPATH=$(dirname $0)/scripts

# Zero parameters list 
ZPARLIST=$(cat $ZPARFILE | grep -v '^\s*#' | cut -f 1 -d ',' | tr '\n' ',')

# Select TOP, RTF, PRM and PSF files
TOP=$(ls $INPDIR | grep '\.rtf$\|\.str$')
RTF=$(ls $FFDIR | grep '\.rtf$')
PRM=$(ls $FFDIR | grep '\.prm$')
PSF=${TOP%.*}.psf

# Test PSF file
if [ ! -f "$INPDIR/$PSF" ]; then
    echo "Error: PSF not generated ..."
    exit 1
fi

# Get FF parameters and MM init energies
PRMFILE=$OUTDIR/all_ff_parameters.prm
cat $FFDIR/$PRM  >  $PRMFILE
cat $INPDIR/$TOP >> $PRMFILE
cp $INPDIR/mm_energies_init.dat $OUTDIR/mm_energies_init.dat

# Calculate energy with fit parameters on zero
echo "Calculating energies in $OUTDIR ..."
PSFFILE=$INPDIR/$PSF
MMINIT=$OUTDIR/mm_energies_init.dat
MMZERO=$OUTDIR/mm_energies_zero.dat
$(dirname $0)/scripts/get_energies_zero.py $PSFFILE $MMINIT $MMZERO $PRMFILE "$ZPARLIST"
rm $PRMFILE



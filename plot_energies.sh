#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: plot_energies.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -w  Working dir

DESCRIPTION:

         - The program loads all energy files in the working dir and makes a plot
           along the scan variable. The plot is showed and saved in working dir.\n"
}

# Options parser
while getopts "w:u" OPTION; do
    case $OPTION in
        w)
            WKDIR=$OPTARG;;
        u)
            UNCONV='YES';;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$WKDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$WKDIR" ]]; then
    echo "Error: $WKDIR is not a directory"; exit 1
fi
if [ -z "$UNCONV" ]; then
    UNCONV='NO'
fi

# Plot energies
echo "Plotting energies in $WKDIR ..."
PDFFILE=$WKDIR/qm_mm_energies.pdf
QMFILE=$WKDIR/qm_energies.dat
MMFILES=''
for DAT in $(ls $WKDIR | grep 'mm_energies.*\.dat'); do
    MMFILES="$MMFILES $WKDIR/$DAT"
done
$(dirname $0)/scripts/plot_energies.py $UNCONV $PDFFILE $QMFILE $MMFILES



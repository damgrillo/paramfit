#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: get_scan_structs.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir
         -o  Output dir

DESCRIPTION:

         - The program extracts structures and energies from QM scan in input dir
           and copies them in PDB format into the output dir. 

         - The QM scan must has been performed using a Gaussian PES scan.

         - The extracted PDB structures and energies will correspond to the 
           stationary points found along the PES scan.

OPTIONS DETAILS:

         -i  Input dir must contain:

             -> LOG files defining the PES scan for a specific parameter.

             -> RTF or STR file defining the topology of the molecule for this scan
               (compatible for CHARMM or CGenFF).
             

         -o  Output dir will contain:

             -> PDB files for each structure obtained from the LOG files for this scan.       
                Atoms and residues are renamed accordingly to match the topology data in RTF/STR file.

             -> QM energy file in tabular format, in the first field the names of the PDB files, and 
                in the second field their corresponding QM energies in Hartree (qm_energies.dat).   

             -> A copy of the topology RTF/STR file is also provided for further usage.\n"

}

# Options parser
while getopts "i:o:" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        o)
            OUTDIR=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" || -z "$OUTDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
elif [[ ! -d "$OUTDIR" ]]; then
    echo "Error: $OUTDIR is not a directory"; exit 1
elif [[ "$(cd $INPDIR && pwd)" == "$(cd $OUTDIR && pwd)" ]]; then
    echo -n "Error: Input dir and output dir are the same. "
    echo "Please choose different location for output dir."
    exit 1
fi

# Export path to extra python modules
export PYTHONPATH=$(dirname $0)

INPDIR=${INPDIR%/}
OUTDIR=${OUTDIR%/}

# Get structures from log files
echo "Extracting structures from $INPDIR ..."
$(dirname $0)/scripts/get_scan_structs.py $INPDIR $OUTDIR

# Select topology file
echo -n "Selecting TOP file in $INPDIR ... "
TOP=$(ls $INPDIR | grep '\.rtf$\|\.str$')
if [ -z "$TOP" ]; then
    echo
    echo "Error: No TOP file found"
    echo "Removing $OUTDIR ... "
    echo "Exit program ..."
    rm -r $OUTDIR
    exit 1
elif [ $(echo $TOP | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several TOP files found"
    echo "Removing $OUTDIR ... "
    echo "Exit program ..."
    rm -r $OUTDIR
    exit 1
elif [ $(cat $INPDIR/$TOP | grep "RESI" | wc -l) -gt 1  ]; then
    echo $TOP
    echo "Error: Several residues defined within $TOP"
    echo "Removing $OUTDIR ... "
    echo "Exit program ..."
    rm -r $OUTDIR
    exit 1
else
    echo "$TOP"
    cp $INPDIR/$TOP $OUTDIR
fi

# Convert structures to pdb using OpenBabel
echo "Converting structures in $OUTDIR to pdb ..."
for XYZ in $(ls $OUTDIR | grep '\.xyz' | sort); do
    PDB=${XYZ%.xyz}.pdb
    obabel $OUTDIR/$XYZ -O $OUTDIR/$PDB &> /dev/null
    VALID_TOP=$($(dirname $0)/scripts/validate_top.py $OUTDIR/$TOP $OUTDIR/$PDB | grep -v 'Error!')
    if [ -n "$VALID_TOP" ]; then
        $(dirname $0)/scripts/rename_pdb.py $OUTDIR/$TOP $OUTDIR/$PDB $OUTDIR/$PDB
        sed -i 's/xyz/pdb/g' $OUTDIR/qm_energies.dat
    else
        echo "Error: Cannot validate TOP for $OUTDIR/$PDB"
        echo "Removing $OUTDIR ... "
        echo "Exit program ..."
        rm -r $OUTDIR
        exit 1
    fi
done
echo 'Done ...'
echo
rm $OUTDIR/*.xyz


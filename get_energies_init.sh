#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: get_energies_init.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -w  Working dir
         -f  Force Field dir

DESCRIPTION:

         - The program runs NAMD for each PDB structure in working dir and saves energies.

         - For energy calculation, NAMD uses the parameters defined in the Force Field dir 
           and in the TOP file included in the working dir.

         - The MM energy file (mm_energies_init.dat) will be in tabular format, in the first field
           the names of the PDB files, in the second field the MM energies in Kcal/mol.  

         - All the PDB structures in the input dir are supposed to have the same 
           topology, which must be defined in the TOP file inside the same dir.

         - TOP file must be a CHARMM-like RTF or STR file.\n"
}

# Options parser
while getopts "w:f:" OPTION; do
    case $OPTION in
        w)
            WKDIR=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$WKDIR" || -z "$FFDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$WKDIR" ]]; then
    echo "Error: $WKDIR is not a directory"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi

# NAMD dir
NAMD=$(dirname $0)/namd

# Clean files
if [ -n "$(ls $WKDIR | grep '_init\.' | grep -v 'energies')" ]; then
    rm $WKDIR/*_init.*
fi
if [ -n "$(ls $WKDIR | grep '_zero\.' | grep -v 'energies')" ]; then
    rm $WKDIR/*_zero.*
fi
if [ -n "$(ls $WKDIR | grep '_new\.' | grep -v 'energies')" ]; then
    rm $WKDIR/*_new.*
fi

# Select TOP file
echo -n "Selecting TOP file in $WKDIR ... "
TOP=$(ls $WKDIR | grep '\.rtf$\|\.str$')
if [ -z "$TOP" ]; then
    echo
    echo "Error: No TOP file found in $WKDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $TOP | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several TOP files found in $WKDIR"
    echo "Exit program ..."
    exit 1
elif [ $(cat $WKDIR/$TOP | grep "RESI" | wc -l) -gt 1  ]; then
    echo $TOP
    echo "Error: Several residues defined within $TOP"
    echo "Exit program ..."
    exit 1
else
    echo "$TOP"
fi

# Select RTF files
echo -n "Selecting RTF file in $FFDIR ... "
RTF=$(ls $FFDIR | grep '\.rtf$')
if [ -z "$RTF" ]; then
    echo
    echo "Error: No RTF file found in $FFDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $RTF | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several RTF files found in $FFDIR"
    echo "Exit program ..."
    exit 1
else
    echo "$RTF"
fi

# Select PRM files
echo -n "Selecting PRM file in $FFDIR ... "
PRM=$(ls $FFDIR | grep '\.prm$')
if [ -z "$PRM" ]; then
    echo
    echo "Error: No PRM file found in $FFDIR"
    echo "Exit program ..."
    exit 1
elif [ $(echo $PRM | wc -w) -gt 1 ]; then
    echo
    echo "Error: Several PRM files found in $FFDIR"
    echo "Exit program ..."
    exit 1
else
    echo "$PRM"
fi

# Selecting PSF file
echo -n "Selecting PSF file in $WKDIR ... "
PSF=$(ls $WKDIR | grep '\.psf$' | head -1)
echo "$PSF"

# Copy FF parameters
TOPINIT=${TOP%.*}_init.${TOP#*.}
PRMINIT=${PRM%.prm}_init.prm
cp $WKDIR/$TOP $WKDIR/$TOPINIT
cp $FFDIR/$PRM $WKDIR/$PRMINIT

# Run NAMD
MMFILE=$WKDIR/mm_energies_init.dat
echo "Running NAMD in $WKDIR ..."
printf "%-14s %9s\n" "# PDB" "MM_Energy"  > $MMFILE
printf "%-14s %9s\n" "# File" "(Kcal/mol)" >> $MMFILE
for PDB in $(ls $WKDIR | grep '\.pdb$'); do
    cp $NAMD/input.namdin $WKDIR
    WKD=$(cd $WKDIR && pwd -P)
    WKD=$(echo $WKD | sed 's/\//\\\//g')
    sed -i "s/%PRM%/$WKD\/$PRMINIT/" $WKDIR/input.namdin
    sed -i "s/%TOP%/$WKD\/$TOPINIT/" $WKDIR/input.namdin
    sed -i "s/%PDB%/$WKD\/$PDB/" $WKDIR/input.namdin
    sed -i "s/%PSF%/$WKD\/$PSF/" $WKDIR/input.namdin
    sed -i "s/%OUT%/$WKD\/namd.out/" $WKDIR/input.namdin
    sed -i "s/%XST%/$WKD\/namd.xst/" $WKDIR/input.namdin
    $NAMD/namd-2.10 $WKDIR/input.namdin > $WKDIR/namd.log
    MME=$(cat $WKDIR/namd.log | grep 'ENERGY' | tail -1 | awk '{printf "%.6f", $12}')
    printf "%-14s %9s\n" "$PDB" "$MME" >> $MMFILE

    # Clean files
    rm $WKDIR/*.log
    rm $WKDIR/*.out.*
    rm $WKDIR/*.namdin
done
rm $WKDIR/$TOPINIT $WKDIR/$PRMINIT
echo



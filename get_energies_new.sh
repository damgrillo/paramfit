#!/bin/bash

# Axiliar function to show usage
usage() {
    echo -e "usage: get_energies_new.sh [OPTIONS]\n"
    echo -e "OPTIONS:
         -i  Input dir
         -o  Output dir
         -p  New parameters file
         -f  Force Field dir

DESCRIPTION:

         - The program gets the MM energies overwriting the new parameters for each PDB structure
           in the input dir and saves energies in the output dir.

         - The MM energy file with new parameters (mm_energies_new.dat) will be in tabular
           format, in the first field the names of the PDB files, in the second field the 
           MM energies in Kcal/mol.   

         - All the PDB structures in the input dir are supposed to have the same 
           topology, which must be defined in the PSF file inside the same dir.\n"
}

# Options parser
while getopts "i:o:p:f:" OPTION; do
    case $OPTION in
        i)
            INPDIR=$OPTARG;;
        o)
            OUTDIR=$OPTARG;;
        p)
            NPARFILE=$OPTARG;;
        f)
            FFDIR=$OPTARG;;
        *)
            usage; exit 1;;
     esac
done

# Mandatory variables
if [[ -z "$INPDIR" || -z "$OUTDIR" || -z "$NPARFILE" || -z "$FFDIR" ]]; then
    usage; exit 1
elif [[ ! -d "$INPDIR" ]]; then
    echo "Error: $INPDIR is not a directory"; exit 1
elif [[ ! -d "$OUTDIR" ]]; then
    echo "Error: $OUTDIR is not a directory"; exit 1
elif [[ ! -f "$NPARFILE" ]]; then
    echo "Error: $NPARFILE is not a file"; exit 1
elif [[ ! -d "$FFDIR" ]]; then
    echo "Error: $FFDIR is not a directory"; exit 1
fi

# Select TOP, RTF, PRM and PSF files
TOP=$(ls $INPDIR | grep '\.rtf$\|\.str$')
RTF=$(ls $FFDIR | grep '\.rtf$')
PRM=$(ls $FFDIR | grep '\.prm$')
PSF=${TOP%.*}.psf

# Test PSF file
if [ ! -f "$INPDIR/$PSF" ]; then
    echo "Error: PSF not generated ..."
    exit 1
fi

# Get FF parameters and MM init energies
PRMFILE=$OUTDIR/all_ff_parameters.prm
cat $FFDIR/$PRM  >  $PRMFILE
cat $INPDIR/$TOP >> $PRMFILE
cp $INPDIR/mm_energies_init.dat $OUTDIR/mm_energies_init.dat

# Calculate energy with new fitted parameters
PSFFILE=$INPDIR/$PSF
echo "Calculating energies in $OUTDIR ..."
MMINIT=$OUTDIR/mm_energies_init.dat
MMNEW=$OUTDIR/mm_energies_new.dat
$(dirname $0)/scripts/get_energies_new.py $PSFFILE $MMINIT $MMNEW $PRMFILE $NPARFILE
rm $PRMFILE


